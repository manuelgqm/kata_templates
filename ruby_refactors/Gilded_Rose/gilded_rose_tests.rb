require File.join(File.dirname(__FILE__), 'gilded_rose')
require 'test/unit'

class TestUntitled < Test::Unit::TestCase

  def test_aged_foo_50_quality
    items = [Item.new("foo", 3, 51)]
    GildedRose.new(items).update_quality()
    assert_equal items[0].quality, 50
  end

  def test_aged_foo_sell_in_minor_0
    items = [Item.new("foo", -3, 51)]
    GildedRose.new(items).update_quality()
    assert_equal items[0].quality, 49
  end

  def test_aged_foo
    items = [Item.new("foo", 0, 10)]
    GildedRose.new(items).update_quality()
    assert_equal items[0].quality, 8
  end

  def test_aged_brie
    items = [Item.new("Aged Brie", 10, 14)]
    GildedRose.new(items).update_quality()
    assert_equal items[0].quality, 15
  end

  def test_aged_brie_sell_in_minor_0
    items = [Item.new("Aged Brie", -3, 14)]
    GildedRose.new(items).update_quality()
    assert_equal items[0].quality, 16
  end

  def test_aged_brie_700_days
    items = [Item.new("Aged Brie", 700, 14)]
    GildedRose.new(items).update_quality()
    assert_equal items[0].quality, 15
  end

  def test_sulfuras
    items = [Item.new("Sulfuras, Hand of Ragnaro", 5, 16)]
    GildedRose.new(items).update_quality()
    assert_equal items[0].quality, 15
  end

  def test_backstage_30_quality
    items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 0, 30)]
    GildedRose.new(items).update_quality()
    assert_equal items[0].quality, 0
  end

  def test_backstage_3_quality
    items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 3, 10)]
    GildedRose.new(items).update_quality()
    assert_equal items[0].quality, 13
  end

  def test_backstage_9_quality
    items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 9, 10)]
    GildedRose.new(items).update_quality()
    assert_equal items[0].quality, 12
  end

  def test_backstage
    items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 12, 5)]
    GildedRose.new(items).update_quality()
    assert_equal items[0].quality, 6
  end

  def test_backstage_50
    items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 3, 50)]
    GildedRose.new(items).update_quality()
    assert_equal items[0].quality, 50
  end

  def test_backstage_55
    items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 7, 55)]
    GildedRose.new(items).update_quality()
    assert_equal items[0].quality, 55
  end

end