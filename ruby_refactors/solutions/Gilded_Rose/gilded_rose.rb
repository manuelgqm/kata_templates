# frozen_string_literal: true

class GildedRose
  def initialize(items)
    @items = items
  end

  def update_quality()
    @items.each do |item|
      product = ProductFactory.create(item)
      item.sell_in = product.reduce_sell_in
      item.quality = product.update_quality
    end
  end
end

class Item
  attr_accessor :name, :sell_in, :quality

  def initialize(name, sell_in, quality)
    @name = name
    @sell_in = sell_in
    @quality = quality
  end

  def to_s()
    "#{@name}, #{@sell_in}, #{@quality}"
  end
end

class Product
  class RequiredMethodNotDefined < StandardError; end
  NORMAL_QUALITY_STEP = 1
  MEDIUM_QUALITY_STEP = 2

  SELL_IN_LIMIT = 0
  SELL_IN_STEP = 1

  def initialize(item)
    @sell_in = item.sell_in
    @quality = item.quality
  end

  def update_quality
    return @quality unless updatable?

    @quality += quality_amount
  end

  def reduce_sell_in
    @sell_in -= SELL_IN_STEP
  end

  private

  def updatable?
    raise RequiredMethodNotDefined
  end

  def quality_amount
    raise RequiredMethodNotDefined
  end

  def sell_in_limit_reached?
    @sell_in <= SELL_IN_LIMIT
  end
end

class ProductFactory
  BACKSTAGE_PASS = "Backstage passes to a TAFKAL80ETC concert"
  BRIE = "Aged Brie"

  def self.create(item)
    case item.name
    when BRIE
      return AgedBrieProduct.new(item)
    when BACKSTAGE_PASS
      return BackstagePassProduct.new(item)
    else
      return NormalProduct.new(item)
    end
  end
end

class NormalProduct < Product
  MIN_QUALITY = 0
  SELL_IN_LIMIT = 0

  def initialize(item)
    super
  end

  private

  def updatable?
    @quality > MIN_QUALITY
  end

  def quality_amount
    return -MEDIUM_QUALITY_STEP if sell_in_limit_reached?

    -NORMAL_QUALITY_STEP
  end
end

class BackstagePassProduct < Product
  HIGH_QUALITY_STEP = 3

  LOW_SELL_IN_THRESHOLD = 10
  HIGH_SELL_IN_THRESHOLD = 5

  MAX_QUALITY = 50

  def initialize(item)
    super
  end

  private

  def updatable?
    incrementable_quality?
  end

  def quality_amount
    return -@quality if sell_in_limit_reached?
    return HIGH_QUALITY_STEP if high_sell_in_threshold_reached?
    return MEDIUM_QUALITY_STEP if low_sell_in_threshold_reached?

    NORMAL_QUALITY_STEP
  end
  
  def high_sell_in_threshold_reached?
    @sell_in <= HIGH_SELL_IN_THRESHOLD
  end

  def low_sell_in_threshold_reached?
    @sell_in <= LOW_SELL_IN_THRESHOLD
  end

  def incrementable_quality?
    @quality < MAX_QUALITY
  end
end

class AgedBrieProduct < Product
  def initialize(item)
    super
  end

  private

  def updatable?
    true
  end

  def quality_amount
    return MEDIUM_QUALITY_STEP if sell_in_limit_reached?

    NORMAL_QUALITY_STEP
  end
end
