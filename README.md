# Kata Templates
This repo contains kata templates for different programming languages.
A Kata it's a programming exercisse to practice Clean Code, Pairing and TDD.

You can find more information at the Merendojo's Wiki page
[Merendojo Wiki Page](https://wiki.tirant.com/wiki/Guía:Merendojos)


## Howto start
Choose a kata and a language then create a copy of the language template folder inside the
`solution/name_of_kata/pair_members_names`. Jump into that new directory and work hard with
your code. Once you're ready, commit your solution and push it to make it available.
## Javascript kata template
Template with babel and jest

### Setup
run in command line
```sh
yarn install
```

### Test
run in command line to execute tests
```sh
yarn test
```

## Ruby kata template

### Setup
run in command line
```sh
bundle install
```

### Test
run in command line to execute tests
```sh
rspec
```

## Python pattern template

### Requirements
[Pipenv](https://github.com/pypa/pipenv)
```sh
pip install pipenv (maybe with sudo)
```

### Setup
run in command line
```sh
pipenv install
```

### Test
run in command line to execute tests
```sh
pipenv run mamba test_spec.py
```


## TODO
- Dockerize
