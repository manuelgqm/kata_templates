require './implementation'
describe 'Greeting' do
  context 'greet' do
    subject { Greeting.greet(target) }

    let(:target) { nil }

    context 'when there is no name' do
      it 'returns a default value' do
        expect(subject).to eq('Hello, my friend.')
      end
    end

    context 'when there is just one name value' do
      let(:target) { 'Paco' }

      it 'returns an interpolation of a name in a greet' do
        expect(subject).to eq("Hello, #{target}.")
      end

      context 'if is in uppercase' do
        let(:target) { 'PACO' }

        it 'shouts the name in a greet' do
          expect(subject).to eq("HELLO #{target}!")
        end
      end
    end

    context 'when there is an array of names' do
      let(:target) { ['Paco', 'Marta'] }

      it 'returns an interpolation of the list of names in a greet' do
        expect(subject).to eq("Hello, #{target.join(' and ')}.")
      end

    end

  end
end
