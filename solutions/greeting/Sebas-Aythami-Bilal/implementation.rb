
class Greeting
  class << self
    def greet(target)
      return 'Hello, my friend.' if target.nil?
      return greet_array_of_names(target) if target.kind_of? Array

      greet_unique_name(target)
    end

    private
      
    def upcase?(target)
      target.upcase == target
    end

    def greet_array_of_names(target)
      return "Hello, #{target.join(' and ')}."
    end

    def greet_unique_name(target)
      return "HELLO #{target}!" if upcase?(target)

      "Hello, #{target}."
    end
  end
end
