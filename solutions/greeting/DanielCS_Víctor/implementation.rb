class Greetings
  class << self
    def say_hi(names = nil)
      obtain_greeting_names names
      obtain_wrapping_greeting


      "#{@greeting}, #{@name_to_greet}#{@end_character}"
    end

    private

    def shouted?
      @name_to_greet.upcase == @name_to_greet
    end

    def obtain_greeting_names(names)
      @name_to_greet = names.nil? ? 'my friend' : names

      if names.kind_of?(Array)
        @name_to_greet = "#{names[0]} and #{names[1]}"
      end
    end

    def obtain_wrapping_greeting
      @greeting = "Hello"
      @end_character = '.'

      if shouted?
        @greeting = @greeting.upcase
        @end_character = '!'
      end
    end
  end
end
