require './implementation'

describe 'Greetings' do
  it 'returns \"Hello, Bob.\" when receives \"Bob\" ' do
    # Arrange
    expected_result = 'Hello, Bob.'
    name = 'Bob'

    # Act
    result = Greetings.say_hi(name)

    # Assert
    expect(result).to eq expected_result
  end

  it 'returns \"Hello, Ana.\" when receives \"Ana\" ' do
    expected_result = 'Hello, Ana.'
    name = 'Ana'

    result = Greetings.say_hi(name)

    expect(result).to eq expected_result
  end

  it 'returns \"Hello, my friend.\" when receives nothing' do
    expected_result = 'Hello, my friend.'

    result = Greetings.say_hi

    expect(result).to eq expected_result
  end

  it 'returns \"HELLO, JERRY!\" when receives uppercase JERRY' do
    expected_result = 'HELLO, JERRY!'
    name = "JERRY"

    result = Greetings.say_hi(name)

    expect(result).to eq expected_result
  end

  it 'greets two people' do
    expected = 'Hello, Jill and Jane.'
    names = ['Jill', 'Jane']

    actual = Greetings.say_hi(names)

    expect(actual).to eq expected
  end
end
