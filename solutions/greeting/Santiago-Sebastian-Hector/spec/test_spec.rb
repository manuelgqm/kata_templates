require './implementation'
describe 'Greeting' do
  it 'should response to greet' do
    expect(Greeting.respond_to?(:greet)).to be_truthy 
  end
  context 'when it has name' do
    let(:name) {'name_example'}
    let(:expected_greeting) {'Hello, name_example.'}
    it 'should interpolate name' do
      expect(Greeting.greet(name)).to eq(expected_greeting)
    end 
  end 
  context 'when name is null' do
    let(:name) {nil}
    let(:expected_greeting) {'Hello, my friend.'}
    it 'should response with generic label' do
      expect(Greeting.greet(name)).to eq(expected_greeting)
    end 
  end
  context 'when name is uppercase' do
    let(:name) {'SANTIAGO'}
    let(:expected_greeting) {'HELLO SANTIAGO!'}
    it 'should response with shout' do
      expect(Greeting.greet(name)).to eq(expected_greeting)
    end
  end
  context 'when name is an array with 2 values' do
    let(:name) {['Hector', 'Sebastian']}
    let(:expected_greeting) {'Hello, Hector and Sebastian.'}
    it 'should response with both names' do
      expect(Greeting.greet(name)).to eq(expected_greeting)
    end
  end 
  context 'when name is a array with + 2 values' do
    let(:name) {['Hector', 'Sebastian', 'Santiago']}
    let(:expected_greeting) {'Hello, Hector, Sebastian and Santiago.'}
    it 'should response with all names' do
      expect(Greeting.greet(name)).to eq(expected_greeting)
    end
  end 
end
