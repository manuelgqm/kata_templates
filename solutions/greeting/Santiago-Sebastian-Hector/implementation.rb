class Greeting
  def self.greet(name)
    return "Hello, my friend." unless name
    return "HELLO #{name}!" if uppcase?(name)
    return "Hello, #{name[0]} and #{name[1]}." if two_names?(name)

    if name.is_a?(Array) && name.length > 2 
      greet = "Hello, " 
      name.each_with_index do |n,i|
        if i == name.length - 1
          greet << "and #{n}."
        elsif i == name.length - 2
          greet << "#{n} "
        else 
          greet << "#{n}, "
        end 
      end 
      return greet
    end 

    "Hello, #{name}."
  end

  private 

  def self.uppcase?(str)
    str.is_a?(String) && /[[:upper:]]/.match(str)
  end 

  def self.two_names?(arr)
    arr.is_a?(Array) && arr.length == 2
  end
end
