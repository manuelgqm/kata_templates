# frozen_string_literal: true

require './implementation'

describe TheGreeting do
  subject do
    TheGreeting.new
  end

  describe '#greet' do
    let(:name) { 'Bob' }
    let(:names) { %w[Jill Jane] }
    let(:array_names_three) { %w[Amy Brian Charlotte] }
    let(:array_names_four) { %w[Amy Brian Charlotte Juan] }
    let(:array_names_five) { %w[Amy Brian Charlotte Juan Marian] }

    let(:array_names_three_shouted_one) { %w[Amy BRIAN Charlotte] }
    let(:array_names_three_shouted_two) { %w[Amy Brian CHARLOTTE] }
    let(:array_names_three_shouted_three) { %w[AMY Brian Charlotte] }

    context 'when a given a one name' do
      it 'should return interpolated string' do
        expect(subject.greet(name)).to eq("Hello, #{name}.")
      end
    end
    context 'when name is null' do
      it 'should return default greet string' do
        expect(subject.greet(nil)).to eq('Hello, my friend.')
      end
    end
    context 'when name is uppercase' do
      it 'should return an uppercase greeting' do
        expect(subject.greet(name.upcase)).to eq("HELLO, #{name.upcase}!")
      end
    end
    context 'when name is an array of two names' do
      it 'should print both names' do
        expect(subject.greet(names)).to eq("Hello, #{names[0]} and #{names[1]}.")
      end
    end
    context 'when name represents more than two names, separate them with commas' do
      it 'should print the last name after "and"' do
        expect(subject.greet(array_names_three)).to eq("Hello, #{array_names_three[0]}, #{array_names_three[1]}, and #{array_names_three[2]}.")
        expect(subject.greet(array_names_four)).to eq("Hello, #{array_names_four[0]}, #{array_names_four[1]}, #{array_names_four[2]}, and #{array_names_four[3]}.")
        expect(subject.greet(array_names_five)).to eq("Hello, #{array_names_five[0]}, #{array_names_five[1]}, #{array_names_five[2]}, #{array_names_five[3]}, and #{array_names_five[4]}.")
      end
    end
    context 'when allow mixing of normal and shouted, separate them with commas' do
      it 'should print shouted names by separating the response into two greetings' do
        expect(subject.greet(array_names_three_shouted_one)).to eq("Hello, #{array_names_three_shouted_one[0]}, and #{array_names_three_shouted_one[2]}. AND HELLO #{array_names_three_shouted_one[1]}!")
        expect(subject.greet(array_names_three_shouted_two)).to eq("Hello, #{array_names_three_shouted_two[0]}, and #{array_names_three_shouted_two[1]}. AND HELLO #{array_names_three_shouted_two[2]}!")
        expect(subject.greet(array_names_three_shouted_three)).to eq("Hello, #{array_names_three_shouted_three[1]}, and #{array_names_three_shouted_three[2]}. AND HELLO #{array_names_three_shouted_three[0]}!")
      end
    end
  end
end
