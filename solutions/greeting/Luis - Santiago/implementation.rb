# Service for kata called 'The Greeting'
class TheGreeting
  def greet(name_input)
    return greeting_more_one_name(name_input) if input_is_array?(name_input)
    return default_greeting if name_input.nil?
    return greeting_name_in_upcase(name_input) if is_upcase?(name_input)

    greeting_unique_name(name_input)
  end

  private

  def greeting_more_one_name(names)
    if names.count == 2
      greet_two_names(names)
    else
      greet_more_names(names)
    end
  end

  def greeting_name_in_upcase(name)
    "HELLO, #{name}!"
  end

  def input_is_array?(array)
    array.is_a?(Array)
  end

  def greet_two_names(names)
    "Hello, #{names[0]} and #{names[1]}."
  end

  def greet_more_names(names)
    if check_if_any_name_is_uppercase(names)
      "#{greet_more_names(list_of_names_in_downcase(names))} AND HELLO #{list_of_names_in_uppercase(names).first}!"
    elsif names.count == 3
      "Hello, #{names[0]}, #{names[1]}, and #{names[2]}."
    else
      new = 'Hello'
      names.each_with_index { |item, index| new << ", #{item}" unless index == names.count - 1 }
      new << ", and #{names[-1]}."
    end
  end

  def list_of_names_in_downcase(array)
    array.reject { |name| name == name.upcase }
  end

  def list_of_names_in_uppercase(array)
    array.select { |name| name == name.upcase }
  end

  def check_if_any_name_is_uppercase(array)
    array.one? { |item| item == item.upcase }
  end

  def greeting_unique_name(name)
    "Hello, #{name}."
  end

  def default_greeting
    'Hello, my friend.'
  end

  def is_upcase?(name)
    name == name.upcase
  end
end
