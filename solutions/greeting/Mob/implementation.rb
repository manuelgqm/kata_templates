class Greeting
  class << self
    def greet(name = 'my friend')
      return 'Hello, my friend.' if name_empty?(name)

      "Hello, #{name}."
    end

    private

    def name_empty?(name)
      name.nil? || name.empty?
    end
  end
end
