require './implementation'

describe 'Greeting' do
  it 'returns Hello, Diego. when receives Diego' do
    expected_greeting = 'Hello, Diego.'
    result = Greeting.greet('Diego')
    expect(result).to eq expected_greeting
  end

  it 'returns Hello, my friend. when receives nil' do
    expected_greeting = 'Hello, my friend.'
    result = Greeting.greet(nil)
    expect(result).to eq expected_greeting
  end

  it 'returns Hello, my friend. when receives no parameters' do
    expected_greeting = 'Hello, my friend.'
    result = Greeting.greet
    expect(result).to eq expected_greeting
  end

  it 'returns Hello, my friend. when receives empty string' do
    expected_greeting = 'Hello, my friend.'
    result = Greeting.greet('')
    expect(result).to eq expected_greeting
  end
end
