require './implementation'

def format_input(input)
  return 'nil' if input.nil?

  input
end

describe 'Greeting' do

  describe '.greet' do
    test_cases = [
      { input: 'Bob', output: 'Hello, Bob.' },
      { input: 'Camila', output: 'Hello, Camila.' },
      { input: nil, output: 'Hello, my friend.' },
      { input: 'JERRY', output: 'HELLO JERRY!' },
      { input: 'TOM', output: 'HELLO TOM!' },
      { input: ['Tom', 'Jerry'], output: 'Hello, Tom and Jerry.' },
      { input: ['Tomas', 'Jeison'], output: 'Hello, Tomas and Jeison.' },
    ]

    test_cases.each do |test_case|
      input = test_case[:input]
      output = test_case[:output]
      context "when input is #{format_input(input)}" do
        it "returns greeting '#{output}'" do
          expect(Greeting.greet(input)).to eq(output)
        end
      end
    end
  end
end
