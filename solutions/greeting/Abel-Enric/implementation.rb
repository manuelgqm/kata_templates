class Greeting
  class << self
    def greet(name)
      name ||= 'my friend'
      return shouting_greeting(name) if uppercase?(name)

      standard_greeting(name)
    end

    private

    def uppercase?(name)
      return false unless name.respond_to?(:upcase)

      name.upcase == name
    end

    def standard_greeting(name)
      name = name.join(' and ') if name.is_a?(Array)

      "Hello, #{name}."
    end

    def shouting_greeting(name)
      "HELLO #{name}!"
    end
  end
end