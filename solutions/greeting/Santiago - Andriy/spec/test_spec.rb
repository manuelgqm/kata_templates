# frozen_string_literal: true

require './implementation'

describe TheGreeting do
  subject do
    TheGreeting.new.greet(names)
  end

  describe '#greet' do
    context 'when a given a one name' do
      let(:names) { 'Bob' }
      it 'should return interpolated string' do
        expected_result = "Hello, #{names}."
        expect(subject).to eq expected_result
      end
    end

    context 'when name is null' do
      let(:names) { nil }
      it 'should return default greet string' do
        expected_result = 'Hello, my friend.'
        expect(subject).to eq expected_result
      end
    end

    context 'when name is uppercase' do
      let(:names) { 'JERRY' }
      it 'should return an uppercase greeting' do
        expected_result = "HELLO #{names}!"
        expect(subject).to eq expected_result
      end
    end

    context 'when is an array of two names' do
      let(:names) { %w[Jill Jane] }
      it 'should print both names' do
        expected_result = "Hello, #{names[0]} and #{names[1]}."
        expect(subject).to eq expected_result
      end
    end

    context 'when is an array more than two names' do
      let(:names) { %w[Amy Brian Charlotte Juan Marian] }
      it 'should print the last name after "and"' do
        expected_result_five_names = "Hello, #{names[0]}, #{names[1]}, #{names[2]}, #{names[3]}, and #{names[4]}."
        expect(subject).to eq expected_result_five_names
      end
    end

    context 'when allow mixing of normal and shouted, separate them with commas' do
      let(:names) { %w[Amy BRIAN Charlotte] }
      it 'should print shouted names by separating the response into two greetings' do
        expected_result_four_names = "Hello, #{names[0]}, and #{names[2]}. AND HELLO #{names[1]}!"
        expect(subject).to eq expected_result_four_names
      end
    end

    context 'when any entries in name are a string containing a comma' do
      let(:names) { ['Bob', 'Charlie, Dianne'] }
      it 'should print amount of names from array' do
        expected_result = 'Hello, Bob, Charlie, and Dianne.'
        expect(subject).to eq expected_result
      end
    end
  end
end
