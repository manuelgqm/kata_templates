require './implementation'

describe 'Gretting' do
  subject { Gretting.greet(name) }

  context 'when name is empty' do
    let(:name) { nil }
    it 'handle empty attributes' do
      expected_greet = 'Hello, my friend.'
      expect(subject).to eq(expected_greet)
    end
  end
  context 'when name is provided' do
    let(:name) { 'Bob' }
    it 'interpolates a name' do
      expected_greet = 'Hello, Bob.'
      expect(subject).to eq(expected_greet)
    end
  end
  context 'when name is shout' do
    let(:name) { 'JERRY' }
    it 'shouts the gretting' do
      expected_greet = 'HELLO, JERRY!'
      expect(subject).to eq(expected_greet)
    end
  end
  context 'when name is an array' do
    let(:name) { %w[Jill Jane] }
    it 'greets all names' do
      expected_greet = 'Hello, Jill and Jane.'
      expect(subject).to eq(expected_greet)
    end
  end
  context 'when name is an array with three names or more' do
    let(:name) { %w[Jill Jane Will] }
    it 'greets all names' do
      expected_greet = 'Hello, Jill, Jane and Will.'
      expect(subject).to eq(expected_greet)
    end
  end
end
