class Gretting
  def self.greet(name)
    name = name.nil? ? 'my friend' : name
    return array_greet(name) if name.instance_of? Array

    return shouting(name) if name_upcase?(name)

    "Hello, #{name}."
  end

  def self.name_upcase?(name)
    name.upcase == name
  end

  def self.shouting(name)
    "HELLO, #{name}!"
  end

  def self.array_greet(name)
    greet = 'Hello'
    name.each_with_index do |n, index|
      return greet += " and #{n}." if index == name.length - 1

      greet += ", #{n}"
    end
  end
end
