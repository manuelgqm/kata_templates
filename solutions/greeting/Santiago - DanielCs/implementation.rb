# Service kata called "The Greeting"
class TheGreeting
  def greet(name)
    if input_is_array?(name)
      greeting_more_one_name(name)
    elsif name.nil?
      greeting_default
    elsif is_upcase?(name)
      greeting_name_in_upcase(name)
    else
      greeting_unique_name(name)
    end
  end

  private

  def greeting_more_one_name(array)
    if there_more_than_one_name_in_quotes?(array)
      separated_by_comma = separate_by_comma(array)
      greet_more_names(separated_by_comma)
    elsif array.count == 2
      greet_two_names(array)
    else
      greet_more_names(array)
    end
  end

  def separate_by_comma(array)
    array
      .select { |name| name.split.size == 1 }
      .concat(
        array
          .select { |name| name.split.size > 1 }
          .join
          .split(',')
          .map(&:strip)
      )
  end

  def there_more_than_one_name_in_quotes?(array)
    array.one? { |name| name.split.size > 1 }
  end

  def greeting_name_in_upcase(string)
    "HELLO #{string}!"
  end

  def input_is_array?(array)
    array.is_a?(Array)
  end

  def greet_two_names(array)
    "Hello, #{array[0]} and #{array[1]}."
  end

  def greet_more_names(array)
    if check_if_any_name_is_uppercase(array)
      "#{greet_more_names(list_of_names_in_downcase(array))} AND HELLO #{list_of_names_in_uppercase(array).first}!"
    else
      greet = 'Hello'
      array.each_with_index { |item, index| greet << ", #{item}" unless index == array.count - 1 }
      greet << ", and #{array[-1]}."
    end
  end

  def list_of_names_in_downcase(array)
    array.reject { |name| name == name.upcase }
  end

  def list_of_names_in_uppercase(array)
    array.select { |name| name == name.upcase }
  end

  def check_if_any_name_is_uppercase(array)
    array.one? { |item| item == item.upcase }
  end

  def greeting_unique_name(string)
    "Hello, #{string}."
  end

  def greeting_default
    'Hello, my friend.'
  end

  def is_upcase?(string)
    string == string.upcase
  end
end
