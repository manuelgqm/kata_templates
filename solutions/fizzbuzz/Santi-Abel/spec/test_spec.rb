require './implementation'
describe 'FIZZBUZZ' do
  subject { FizzBuzz.calculate(number) }

  shared_examples 'Fizz' do
    it 'writes Fizz' do
      is_expected.to eq(expected_response)
    end
  end

  shared_examples 'Buzz' do
    it 'writes Buzz' do
      is_expected.to eq(expected_response)
    end
  end

  shared_examples 'FizzBuzz' do
    it 'writes FizzBuzz' do
      is_expected.to eq(expected_response)
    end
  end

  shared_examples 'Number as String' do
    it 'writes the string representation of the number' do
      is_expected.to eq(expected_response)
    end
  end


  context 'when number includes 3' do
    let(:expected_response) { 'Fizz' }
    let(:number) { 3 }

    include_examples 'Fizz'

    context 'when is divisible by 3' do
      let(:number) { 33 }

      include_examples 'Fizz'
    end

    context 'when is not divisible by 3' do
      let(:number) { 13 }

      include_examples 'Number as String'
    end
  end

  context 'when number includes 5' do
    let(:expected_response) { 'Buzz' }
    let(:number) { 5 }

    include_examples 'Buzz'

    context 'when is divisible by 5' do
      let(:number) { 25 }

      include_examples 'Buzz'
    end

    context 'when is not divisible by 5' do
      let(:number) { 52 }

      include_examples 'Number as String'
    end
  end

  context 'when number is divisible by both 5 and 3' do
    let(:number) { 15 }
    let(:expected_response) { 'FizzBuzz' }

    include_examples 'FizzBuzz'
  end

  context 'when number is non-divisible by neither 5 and 3' do
    let(:number) { 2 }
    let(:expected_response) { '2' }

    include_examples 'FizzBuzz'

    context 'when number contains 5 and 3' do
      let(:number) { 53 }
      let(:expected_response) { 'Fizz' }

      include_examples 'Fizz'
    end
  end
end
