class FizzBuzz
  def self.calculate(number)
    divisible_by_three = (number % 3).zero?
    divisible_by_five = (number % 5).zero?
    includes_three = number.to_s.include? '3'
    includes_five = number.to_s.include? '5'
    # includes_both = includes_three && includes_five

    return 'FizzBuzz' if divisible_by_three && divisible_by_five

    return 'Fizz' if divisible_by_three || includes_three

    return 'Buzz' if divisible_by_five || includes_five

    number.to_s
  end

  private

  # def divisible_by_three
  #   @divisible_by_three ||= (number % 3).zero?
  # end
end
