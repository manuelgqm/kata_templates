require './implementation'

def array_result_of_100
  %w[1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz 16 17
    Fizz 19 Buzz Fizz 22 23 Fizz Buzz 26 Fizz 28 29 FizzBuzz 31 32
    Fizz 34 Buzz Fizz 37 38 Fizz Buzz 41 Fizz 43 44 FizzBuzz 46 47
    Fizz 49 Buzz Fizz 52 53 Fizz Buzz 56 Fizz 58 59 FizzBuzz 61 62
    Fizz 64 Buzz Fizz 67 68 Fizz Buzz 71 Fizz 73 74 FizzBuzz 76 77
    Fizz 79 Buzz Fizz 82 83 Fizz Buzz 86 Fizz 88 89 FizzBuzz 91 92
    Fizz 94 Buzz Fizz 97 98 Fizz Buzz]
end

describe 'FizzBuzz' do 
  context 'list_numbers' do
    context 'without parameters' do
      subject { FizzBuzz.list_numbers }

      it 'return a Array' do
        is_expected.to be_an_instance_of(Array)
      end

      it 'return an Array of 100 elements' do
        expect(subject.length).to eq(100)
      end

      context 'when numbers 11..16' do
        it 'returns ["BUZZ","11","FIZZ","FIZZ","14", "FIZZBUZZ"]' do
          expected_list = ['BUZZ','11','FIZZ','FIZZ','14', 'FIZZBUZZ']
          expect(FizzBuzz.list_numbers[9,6]).to eq(expected_list)
        end 
      end
    end

    context 'with parameters' do
      subject { FizzBuzz.list_numbers(from, to) }
      let(:from) { 1 }
      let(:to) { 20 }
      it 'return a Array' do
        is_expected.to be_an_instance_of(Array)
      end
  
      context 'when from value is greater than to value' do
        let(:from) { 20 }
        let(:to) { 10 }
        it 'return an empty Array' do
          is_expected.to be_empty
        end
      end

      context 'when from is 10 and to is 20' do
        let(:from) { 10 }
        let(:to) { 20 }
        it 'returns an Array of 11 elements' do
          expect(subject.length).to eq(11)
        end
        it "returns ['BUZZ','11','FIZZ','FIZZ','14', 'FIZZBUZZ', 16, 17, 'FIZZ', 19, 'BUZZ']" do
          expected_list = ['BUZZ','11','FIZZ','FIZZ','14', 'FIZZBUZZ', '16', '17', 'FIZZ', '19', 'BUZZ']
          is_expected.to eq(expected_list)
        end
      end
    end
  end
end


