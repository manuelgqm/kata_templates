class FizzBuzz
  def self.list_numbers(from = 1, to = 100)
    require 'byebug'; byebug
    (from..to).map { |number| fizzbuzz(number) }
  end
  
  private

  def self.fizzbuzz(number)
    str = ''
    str += 'FIZZ' if is_fizz_buzz(number, 3)
    str += 'BUZZ' if is_fizz_buzz(number, 5)
    str.empty? ? number.to_s : str
  end

  def self.is_fizz_buzz(number_compared, divider)
    return true if number_compared % divider == 0
    return true if number_compared.to_s.include? divider.to_s
    return false
  end
end