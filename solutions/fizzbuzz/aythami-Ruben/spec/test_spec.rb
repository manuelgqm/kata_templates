require './implementation'
require 'byebug'

describe FizzBuzz do
  subject { FizzBuzz.new }

  shared_examples 'fizzBuzz' do |input_number, expected_result|
    it 'returns the right value' do
      expect(subject.calculate(input_number)).to eq expected_result
    end
  end

  it 'Should exists a FizzBuzz' do
    expect(subject).to be_a FizzBuzz
  end

  describe 'checks all results' do
    expected_result = [1, 2, 'Fizz', 4, 'Buzz', 'Fizz', 7, 8, 'Fizz', 'Buzz', 11, 'Fizz', 13, 14, 'FizzBuzz']
    (0..14).each_with_index do |input_value, index|
      context 'should return the value correct' do
        include_examples('fizzBuzz', input_value + 1, expected_result[index])
      end
    end
  end
end
