class FizzBuzz
  def calculate(number)
    return 'FizzBuzz' if fizzbuzz?(number)
    return 'Fizz' if fizz?(number)
    return 'Buzz' if buzz?(number)

    number
  end

  private

  def fizz?(number)
    (number % 3).zero?
  end

  def buzz?(number)
    (number % 5).zero?
  end

  def fizzbuzz?(number)
    fizz?(number) && buzz?(number)
  end
end
