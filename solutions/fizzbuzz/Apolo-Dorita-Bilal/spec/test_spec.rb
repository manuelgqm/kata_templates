require './implementation'
describe 'FizzBuzz' do
  context 'fizzbuzz' do
    subject! { FizzBuzz.fizzbuzz(number) }

    context 'when the param is a 1' do
      let(:number) { 1 }
      it 'returns 1' do
        is_expected.to eq('1')
      end
    end

    context 'when is divisible by 3 ' do
      let(:number) { 6 }
      it 'returns FIZZ' do
        is_expected.to eq('FIZZ')
      end
    end

    context 'when is divisible by 5 ' do
      let(:number) { 5 }
      it 'returns BUZZ' do
        is_expected.to eq('BUZZ')
      end
    end

    context 'when number is divisible by 3 and 5' do
      let(:number) { 15 }
      it 'returns FIZZBUZZ' do
        is_expected.to eq('FIZZBUZZ')
      end
    end
  end

  context 'is_fizz' do
    context 'when is divisible by 3 ' do
      let(:number) { 6 }
      it 'returns true' do
        expect(FizzBuzz.is_fizz(number)).to be_truthy
      end
    end

    context 'when has a 3 in it' do
      let(:number) { 13 }
      it 'returns FIZZ' do
        expect(FizzBuzz.is_fizz(number)).to be_truthy
      end
    end

    context 'when has not a 3 in it or is not divisible by 3' do
      let(:number) { 16 }
      it 'returns FIZZ' do
        expect(FizzBuzz.is_fizz(number)).to be_falsey
      end
    end
  end

  context 'is_buzz' do
    context 'when is divisible by 5' do
      let(:number) { 10 }
      it 'returns true' do
        expect(FizzBuzz.is_buzz(number)).to be_truthy
      end
    end

    context 'when has a 5 in it' do
      let(:number) { 15 }
      it 'returns BUZZ' do
        expect(FizzBuzz.is_buzz(number)).to be_truthy
      end
    end

    context 'when has not a 5 in it' do
      let(:number) { 16 }
      it 'returns FIZZ' do
        expect(FizzBuzz.is_buzz(number)).to be_falsey
      end
    end
  end

  context 'list_numbers' do
    it 'return a Array' do  
      expect(FizzBuzz.list_numbers).to be_an_instance_of(Array)
    end

    context 'when numbers 11..16' do
      let(:list_returned) { ['BUZZ','11','FIZZ','FIZZ','14', 'FIZZBUZZ'] }
      it 'returns ["BUZZ","11","FIZZ","FIZZ","14", "FIZZBUZZ"]' do
        expect(FizzBuzz.list_numbers[9,6]).to eq(list_returned)
      end 
    end
  end
end