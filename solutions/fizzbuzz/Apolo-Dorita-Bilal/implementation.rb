class FizzBuzz
  def self.list_numbers
    (1..100).map { |number| fizzbuzz(number) }
  end

  def self.fizzbuzz(number)
    return 'FIZZBUZZ' if number % 3 == 0 && number % 5 == 0
    return 'FIZZ' if is_fizz(number)
    return 'BUZZ' if is_buzz(number)
    number.to_s
  end

  def self.is_fizz(number)
    return true if number % 3 == 0
    return true if number.to_s.include? '3'
    return false
  end

  def self.is_buzz(number)
    return true if number % 5 == 0
    return true if number.to_s.include? '5'
    return false
  end

  
end