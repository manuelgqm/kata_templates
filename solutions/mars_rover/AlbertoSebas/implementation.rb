class MarsRover
  attr_reader :x_coord, :y_coord, :orientation

  def initialize(x_coord: nil, y_coord: nil, orientation: nil)
    @x_coord = x_coord
    @y_coord = y_coord
    @orientation = orientation
    @orientation_integer = orientation_to_integer
  end

  def orientation_to_integer
    case @orientation
    when 'E'
      0
    when 'N'
      90
    when 'W'
      180
    when 'S'
      270
    end
  end

  def integer_to_orientation(integer_degree)
    case integer_degree
    when 0
      'E'
    when 90
      'N'
    when 180
      'W'
    when 270
      'S'
    end
  end

  def command(command_instruction)
    degree_move = orientation_to_integer
    command_instruction.each_char do |instruction|
      case instruction
      when 'M'
        case orientation
        when 'N'
          @y_coord += 1
        when 'S'
          @y_coord -= 1
        when 'W'
          @_coord -= 1
        when 'E'
          @_coord += 1
        end
      when 'R'
        degree_move += 90
        degree_move = 0 if degree_move == 270
        p '90 right'
      when 'L'
        p '90 left'
      end
    end
    @orientation = integer_to_orientation(degree_move)
  end
end
