require './implementation'
describe 'MarsRover' do
  let(:mars_rover) { MarsRover.new(x_coord: 5, y_coord: 5, orientation: 'N') }
  context 'when recives empty command' do
    subject! { mars_rover.command('') }
    describe 'does not move' do
      it 'x position' do
        expect(mars_rover.x_coord).to eq 5
      end
      it 'y position' do
        expect(mars_rover.y_coord).to eq 5
      end
      it 'orientation' do
        expect(mars_rover.orientation).to eq 'N'
      end
    end
  end

  context 'when recives a list of letters as command' do
    subject! { mars_rover.command(command) }
    describe 'and recives only M' do
      let(:command) { 'MMM' }
      let(:x_expect) { 5 }
      let(:y_expect) { 8 }
      let(:orientation_expect) { 'N' }

      it 'moves correctly on x position' do
        expect(mars_rover.x_coord).to eq x_expect
      end
      it 'moves correctly on y position' do
        expect(mars_rover.y_coord).to eq y_expect
      end
      it 'moves correctly on orientation' do
        expect(mars_rover.orientation).to eq orientation_expect
      end
    end

    describe 'and receives R' do
      let(:command) { 'R' }
      let(:x_expect) { 5 }
      let(:y_expect) { 5 }
      let(:orientation_expect) { 'W' }

      it 'x position stays as initial' do
        expect(mars_rover.x_coord).to eq x_expect
      end
      it 'y position stays as initial ' do
        expect(mars_rover.y_coord).to eq y_expect
      end
      it 'orientation changes' do
        expect(mars_rover.orientation).to eq orientation_expect
      end
    end

    # describe 'and receives L' do
    #   let(:command) { 'MMM' }
    #   let(:x_expect) { 5 }
    #   let(:y_expect) { 8 }
    #   let(:orientation_expect) { 'N' }

    #   it 'x position' do
    #     expect(mars_rover.x_coord).to eq 5
    #   end
    #   it 'y position' do
    #     expect(mars_rover.y_coord).to eq 8
    #   end
    #   it 'orientation' do
    #     expect(mars_rover.orientation).to eq 'N'
    #   end
    # end
  end
end
