require './implementation'
describe Mars do

  let(:rover) { described_class.new(position) }
  let(:position) { [5, 5, 'N'] }
  let(:command) {''}

  describe 'landing initial position' do
    it 'with empty command return landing position' do
      expect(rover.move(command)).to eq position
    end
  
    it 'with "M" command advance forward' do
      command = 'M'
      expect(rover.move(command)).to eq [5,6,'N']
    end

    it 'with any numbers of "M" command advance that number of steps forward' do
      command = 'MMM'
      expect(rover.move(command)).to eq [5,8,'N']
    end
  end

  describe 'turns' do

    it 'with "R" command 90 degrees right' do
      command = 'R'
      expect(rover.move(command)).to eq [5,5,'E']
    end

    it 'with "L" command 90 degrees left' do
      command = 'L'
      expect(rover.move(command)).to eq [5,5,'W']
    end

  end
end
