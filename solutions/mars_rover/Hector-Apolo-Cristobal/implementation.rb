class Mars
  @cardinal_points = ['N','W','S','E']
  def initialize(position)
    @landing = position
  end

  def move(commands)
    return @landing if commands == ''

    final_position = @landing
    commands.split('').each do |step|
      if step == 'M'
        final_position[1] += 1
      else
        #current_position = @cardinal_points.index step +1
        #final_position = @cardinal_points[current_position%4]
        if step == 'R'
          final_position[2] = 'E'
          else
            if step == 'L'
              final_position[2] = 'W'
          end
        end
      end
    end
    final_position
  end
end
