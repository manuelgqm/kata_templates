require './implementation'
describe 'Robot' do
  subject do
    robot = Robot.new
    robot.set_landing_position(landing_position)
    robot.landing(comands)
  end
  let (:landing_position) {[5, 5, 'N']}
  let (:comands) {''}

  it 'knows it landing zone' do
    is_expected.to eq([5, 5, 'N'])
  end

  describe 'when it lands in [1, 2, N] And has comands parameters' do
    let (:landing_position) {[1, 2, 'N']}
    let (:comands) {'MMM'}
    it 'landing position changes 3 meters north' do
      is_expected.to eq([1, 5, 'N'])
    end
  end
end