require './implementation'

describe 'The Mars Rover' do
  describe 'can land' do
    subject! { MarsRover.land(landing_position) }

    shared_examples 'in expected position' do
      it "and it's on expected position" do
        expect(MarsRover.position).to eq landing_position
      end
    end

    context 'in position "5,5,N"' do
      let(:landing_position) { "5,5,N" }

      include_examples "in expected position"
    end

    context 'in position "5,4,N"' do
      let(:landing_position) { "5,4,N" }

      include_examples "in expected position"
    end
  end

  describe 'run command' do
    
    it 'when recive a M value it move' do
      land_position = '1,2,N'
      command = 'M'
      final_position = '1,3,N'
      MarsRover.land(land_position)
      MarsRover.excecute_command(command)
      expect(MarsRover.position).to eq(final_position)
    end

    it 'when recive a M value it move' do
      land_position = '1,2,N'
      command = 'MM'
      final_position = '1,4,N'
      MarsRover.land(land_position)
      MarsRover.excecute_command(command)
      expect(MarsRover.position).to eq(final_position)
    end
  end
end
