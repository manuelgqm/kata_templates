class MarsRover
  class << self
    def land(position)
      @position = position
    end

    def position
      @position
    end

    def excecute_command(command)
      position = @position.split(',')
      list_command = command.split('')
      list_command.each do |command|
        if command == 'M'
          position[1] = position[1].to_i + 1
        end
      end
      @position = position.join(',')
    end
  end

end