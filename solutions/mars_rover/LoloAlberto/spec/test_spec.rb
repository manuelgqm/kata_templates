require './implementation'
describe 'Mars Rover' do
  let(:rover) { Rover.new(*landing_position) }

  describe 'knows its initial position' do
    subject { rover.landing_position}

    context 'at 5,5,N' do
      let(:landing_position) { [5,5,'N'] }

      it { is_expected.to eq "5,5,N" }
    end

    context 'at 6,5,N' do
      let(:landing_position) { [6,2,'W'] }

      it { is_expected.to eq "6,2,W" }
    end
  end

  describe 'accepts commands' do
    subject! { rover.command_run(command) }

    let(:command) { '' }
    let(:landing_position) { [5,5,'N'] }

    describe 'empty command' do
      it 'does not move' do
        expect(rover.current_position).to eq rover.landing_position
      end
    end

    describe 'when receives "M"' do
      let(:command) { 'M' }

      it 'moves forward one step' do
        expect(rover.current_position).to eq "5,6,N"
      end
    end

    describe 'when receives "MM"' do
      let(:command) { 'MM' }

      it 'moves forward two steps' do
        expect(rover.current_position).to eq "5,7,N"
      end
    end

    describe 'when receives "R"' do
      let(:command) { 'R' }

      it 'turns to the right' do
        expect(rover.current_position).to eq "5,5,E"
      end
    end

    describe 'when receives "RR"' do
      let(:command) { 'RR' }

      it 'turns to the right twice' do
        expect(rover.current_position).to eq "5,5,S"
      end
    end

    describe 'when receives "L"' do
      let(:command) { 'L' }

      it 'turns to the left' do
        expect(rover.current_position).to eq "5,5,W"
      end
    end

    describe 'when receives "LL"' do
      let(:command) { 'LL' }

      it 'turns to the right twice' do
        expect(rover.current_position).to eq "5,5,S"
      end
    end

    describe 'when receives "LM"' do
      let(:command) { 'LM' }

      it 'turns to the right twice' do
        expect(rover.current_position).to eq "4,5,W"
      end
    end
  end
end
