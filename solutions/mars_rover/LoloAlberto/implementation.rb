class Rover
  def initialize(x, y, orientation)
    @landing_position = @current_position = [x, y, orientation]
  end

  def landing_position
    @landing_position.map(&:to_s).join(',')
  end

  def current_position
    @current_position.map(&:to_s).join(',')
  end

  def command_run(command)
    command.chars.each do |char|
      case char
      when 'R'
        self.orientation = Rover.right_movements_mapping[orientation.to_sym]
      when 'L'
        self.orientation = Rover.left_movements_mapping[orientation.to_sym]
      else
        case orientation
        when 'N'
          increment_coord_y
        when 'E'
          increment_coord_y
        when 'S'
          increment_coord_y
        when 'W'
          decrement_coord_x
        end
      end
    end
  end

  private

  def orientation=(value)
    @current_position[2] = value
  end

  def orientation
    @current_position[2]
  end

  def increment_coord_y
    @current_position[1] += 1
  end

  def decrement_coord_x
    @current_position[0] -= 1
  end

  def self.right_movements_mapping
    {
      N: 'E',
      E: 'S',
      S: 'W',
      W: 'N'
    }
  end

  def self.left_movements_mapping
    {
      N: 'W',
      E: 'N',
      S: 'E',
      W: 'S'
    }
  end
end
