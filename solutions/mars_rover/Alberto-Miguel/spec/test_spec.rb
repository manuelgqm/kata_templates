require './implementation'
describe 'MarsRover' do
  subject do
    MarsRover.landing_position(initial_position)
  end

  let(:initial_position) { '5,5,N' }

  it 'knows the landing position' do
    expect(subject).to eq('5,5,N')
  end

  describe '#execute_command' do
    subject do
      MarsRover.landing_position(initial_position)
      MarsRover.execute_command(command)
      MarsRover.position
    end

    context 'when it receives a blank command' do
      let(:command) { '' }

      it 'current position is the same' do
        expect(subject).to eq('5,5,N')
      end
    end

    context 'when it receives a single M' do
      let(:command) { 'M' }

      it 'moves 1 step forward' do
        expect(subject).to eq('5,6,N')
      end
    end

    context 'when it receives multiple Ms' do
      let(:command) { 'MMM' }

      it 'moves 3 step forward' do
        expect(subject).to eq('5,8,N')
      end
    end

    context 'when it receives a single R' do
      let(:command) { 'R' }

      it 'turns right' do
        expect(subject).to eq('5,5,E')
      end
    end

    context 'when it receives multiple Rs' do
      let(:command) { 'RRR' }

      it 'turns right 3 times' do
        expect(subject).to eq('5,5,W')
      end
    end

    context 'when it receives a single L' do
      let(:command) { 'L' }

      it 'turns left' do
        expect(subject).to eq('5,5,W')
      end
    end

    context 'when it receives multiple Ls' do
      let(:command) { 'LLL' }

      it 'turns left 3 times' do
        expect(subject).to eq('5,5,E')
      end
    end

    context 'when it receives mixed orders' do
      let(:command) { 'MLM' }

      it 'moves forward, turns left and moves forward again' do
        expect(subject).to eq('4,6,W')
      end
    end

    context 'when the rover is looking at the south' do
      let(:initial_position) { '5,5,S' }
      let(:command) { 'MLM' }

      it 'moves forward, turns left and moves forward again' do
        expect(subject).to eq('6,4,E')
      end
    end

    context 'when the rover is looking at the west' do
      let(:initial_position) { '1,1,W' }
      let(:command) { 'MMLMMRMRML' }

      it 'moves and turns according to the command' do
        expect(subject).to eq('-2,0,W')
      end
    end
  end
end
