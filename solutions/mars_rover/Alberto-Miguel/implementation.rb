class MarsRover
  COORDINATES = ['W','N','E','S'].freeze

  class << self
    def landing_position(initial_position)
      @pos_x, @pos_y, @orientation = initial_position.split(',')
      position
    end

    def execute_command(command)
      command.each_char do |order|
        if order == 'M'
          case @orientation
          when 'N'
            @pos_y = @pos_y.to_i + 1
          when 'S'
            @pos_y = @pos_y.to_i - 1
          when 'E'
            @pos_x = @pos_x.to_i + 1
          when 'W'
            @pos_x = @pos_x.to_i - 1
          end
        elsif order == 'R'
          @orientation = rotate(:right)
        elsif order == 'L'
          @orientation = rotate(:left)
        end
      end
    end

    def position
      [@pos_x, @pos_y, @orientation].join(',')
    end

    def rotate(direction)
      direction_index = direction == :right ? 1 : -1
      current_coordinate = COORDINATES.index(@orientation)
      new_coordinate = (current_coordinate + direction_index) % 4
      @orientation = COORDINATES[new_coordinate]
    end
  end
end
