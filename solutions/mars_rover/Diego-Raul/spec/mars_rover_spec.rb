require './lib/mars_rover'

RSpec.describe MarsRover do
  describe 'Mars Rover' do
    it 'returns the landing position' do
      position = [5, 5, 'N']
      rover = MarsRover.new(position)

      rover.command('')

      expect(rover.current_position).to eq(position)
    end

    it 'moves forward when recieves M command' do
      position = [1, 2, 'N']
      rover = MarsRover.new(position)

      rover.command('MMM')

      expect(rover.current_position).to eq([1, 5, 'N'])
    end

    it 'turns right when recieves R command' do
      position = [1, 2, 'N']
      rover = MarsRover.new(position)

      rover.command('RR')

      expect(rover.current_position).to eq([1, 2, 'Z'])
    end

    it 'turns left when recieves L command' do
      position = [1, 2, 'N']
      rover = MarsRover.new(position)

      rover.command('L')

      expect(rover.current_position).to eq([1, 2, 'W'])
    end
  end
end
