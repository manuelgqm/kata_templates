class MarsRover
  def initialize(landing)
    @x_position = landing.first
    @y_position = landing[1]
    @direction = landing.last

    @cardinal_directions = ['N', 'E', 'S', 'W']
  end

  def command(instructions)
    #each_char
    instructions.split('').each do |instruction|
      @y_position += 1 if instruction == 'M'
      if instruction == 'R'
        @direction = turn_right
      end
      @direction = 'W' if instruction == 'L'
    end
  end

  def current_position
    [@x_position, @y_position, @direction]
  end

  def turn_right
    index_position = @cardinal_directions.find_index(@direction)
  end
end
