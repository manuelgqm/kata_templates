class MarsRover
  def self.landing(position)
    @position = position
  end

  def self.final_zone(commands)
    return @position if commands.empty?

    coord_x, coord_y, orientation = @position.split(',')
    commands.each_char{ |command|
      if command == 'M'
        coord_y = coord_y.to_i + 1      
      elsif command == 'L'
        orientation = 'W'
      elsif command == 'R'
        orientation = 'E'
      end
    }

    return coord_x.to_s + ',' + coord_y.to_s + ',' + orientation.to_s
  end
end
