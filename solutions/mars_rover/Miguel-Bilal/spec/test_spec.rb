require './implementation'
describe 'MarsRover' do
  
  subject {
    MarsRover.final_zone(commands)
  }

  let(:position) { '5,5,N' }
  let(:commands) { '' }

  before do
    MarsRover.landing(position)
  end

  describe 'when there are no commands' do
    it 'returns the current location' do
      is_expected.to eq(position)
    end
  end

  describe 'when there are commands' do
  
    let(:position) { '1,2,N' }
    
    describe 'when only one step' do
      describe 'forward' do
        let(:commands) { 'M' }
        it 'M command is sent' do
          is_expected.to eq('1,3,N')
        end
      end

      describe 'left' do
        let(:commands) { 'L' }
        it 'L command is sent' do
          is_expected.to eq('1,2,W')
        end
      end

      describe 'right' do
        let(:commands) { 'R' }
        it 'R command is sent' do
          is_expected.to eq('1,2,E')
        end
      end

    end

    describe 'when multiple step forward commands' do

      let(:commands) { 'MMM' }

      it 'MMM command is sent' do
        is_expected.to eq('1,5,N')
      end
    end
  end
end
