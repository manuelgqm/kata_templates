require './implementation'
describe 'Mars rover' do
  subject! do
    move(landing_position, command)
  end
  describe 'when command is empty' do
    let(:landing_position) { '5, 5, N' }
    let(:command) { '' }
    expected_position = '5, 5, N'

    it 'is located on the initial position' do
      is_expected.to eq(expected_position)
    end
  end

  describe 'when command is not empty' do
    let(:landing_position) { '1, 2, N' }
    let(:command) { 'MMM' }
    expected_position = '1, 5, N'

    it 'moves three places forward when command is MMM' do
      is_expected.to eq(expected_position)
    end
  end
end


# Green belt
# The rover knows its final position.
# The rover receives a list of letters as command.
# When the rover recives the command "M" it moves forward.
# When the rover recives the command "R" it turns 90 degrees right.
# When the rover recives the command "L" it turns 90 degrees left.

# Sample code:
# Landing position: 1, 2, N
# Command: "MMM"
# Final position: 1, 5, N