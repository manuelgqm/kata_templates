require './implementation'
describe 'MarsRover' do
  let(:mars_rover) { MarsRover.new([5, 5, 'N']) }

  it 'lands on same position if command ''' do
    expect(mars_rover.command('')).to eq([5, 5, 'N'])
  end

  it "moves forward if receives 'M' " do
    expect(mars_rover.command('M')).to eq([5, 6, 'N'])
  end

  it "moves to right if receives 'R' " do
    expect(mars_rover.command('R')).to eq([5, 5, 'E'])
  end

  it "moves 360 degree if receives 'RRRR'" do
    expect(mars_rover.command('RRRR')).to eq([5, 5, 'N'])
  end

  it "moves to LEFT if receives 'L' " do
    expect(mars_rover.command('L')).to eq([5, 5, 'W'])
  end

  it "moves 360 degree if receives 'LLLL'" do
    expect(mars_rover.command('LLLL')).to eq([5, 5, 'N'])
  end

  it "moves forward 3 times if receives 'MMM'" do
    expect(mars_rover.command('MMM')).to eq([5, 1, 'N'])
  end

  it "goes back to 0 if passes the world edge on 'y' axe" do
    expect(mars_rover.command('MM')).to eq([5, 0, 'N'])
  end

  it "goes back to 0 if passes the world edge on 'x' axe" do
    expect(mars_rover.command('RMM')).to eq([0, 5, 'E'])
  end

  it "decrease y position if is oriented to 'S'" do
    expect(mars_rover.command('RRMM')).to eq([5, 3, 'S'])
  end

  it "decrease x position if is oriented to 'W'" do
    expect(mars_rover.command('LMM')).to eq([3, 5, 'W'])
  end
end
