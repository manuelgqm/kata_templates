class MarsRover
  attr_accessor :coords

  ORIENTATIONS = ["S", "W", "N", "E"]
  WORLD_DIMENSIONS = [6, 6]

  def initialize(coords)
    @coords = coords
    @actual_orientation = ORIENTATIONS.index(coords[2])
  end

  def command(command)
    command.each_char do |char_command|
      case char_command
      when 'M'
        if @actual_orientation > 1
          @coords[direction_axe] += 1
        else
          @coords[direction_axe] -= 1
        end

        @coords[direction_axe] = 0 if @coords[direction_axe] > WORLD_DIMENSIONS[direction_axe]
      when 'R'
        @actual_orientation = (@actual_orientation + 1) % 4
      when 'L'
        @actual_orientation = (@actual_orientation - 1) % 4
      end
    end
    @coords[2] = ORIENTATIONS[@actual_orientation]
    @coords
  end

  private

  def direction_axe
    (@actual_orientation + 1) % 2
  end
end
