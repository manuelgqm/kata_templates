class MarsRovers
  def self.land(position)
    @coord_x, @coord_y, @direction = position.split(',')
  end

  def self.command(command)
    command.each_char{ |char|
      if char == 'M'
        @coord_y = @coord_y.to_i + 1
      end
    }
    
    position_concat
  end

  def self.position_concat
    @coord_x.to_s + ',' +@coord_y.to_s+ ',' + @direction.to_s
  end
end
