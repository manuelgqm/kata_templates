require './implementation'
describe 'Mars Rover' do
  let(:position) { '5,5,N' }
  let(:command) { '' }

  subject do
    MarsRovers.land(position)
    MarsRovers.command(command)
  end

  describe 'when there is not command assignement' do
    it 'Landing position and final position are the same' do
      is_expected.to eq position
    end
  end

  describe 'when there is a command assignement' do
    let(:final_position) { '1,5,N'}
    describe 'forward' do
      describe 'and it moves 3 steps when command is MMM' do
        let(:position) { '1,2,N' }
        let(:command) { 'MMM' }
        it 'returns 1,5,N when command is MMM' do
          is_expected.to eq final_position
        end
      end
      describe 'and it moves 1 steps when command is M' do
        let(:position) { '1,2,N' }
        let(:command) { 'M' }
        let(:final_position) { '1,3,N' }
        it 'returns 1,3,N when command is M' do
          is_expected.to eq final_position
        end
      end
    end
  end
end
