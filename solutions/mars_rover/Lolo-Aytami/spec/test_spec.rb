require './implementation'
describe 'The Mars Rover' do
  describe 'can land' do
    subject { MarsRover.land(landing_position) }
    
    before { subject }

    shared_examples 'is in expected position' do
      it "and it's on expected position" do 
        expect(MarsRover.position).to eq landing_position
      end
    end
    
    context 'in position "5,5,N"' do
      let(:landing_position) { "5,5,N" }

      include_examples "is in expected position"
    end
    
    context 'in position "5,4,N"' do
      let(:landing_position) { "5,4,N" }
      
      include_examples "is in expected position"
    end
  end

  describe 'perform a command' do
    it 'when receives "M" it moves forward' do
      landing_position = "1,2,N"
      command = "M"
      expected_position = "1,3,N"

      MarsRover.land(landing_position)
      MarsRover.perform(command)

      expect(MarsRover.position).to eq expected_position
    end

    it 'when receives "M" it moves forward' do
      landing_position = "1,1,N"
      command = "M"
      expected_position = "1,2,N"

      MarsRover.land(landing_position)
      MarsRover.perform(command)

      expect(MarsRover.position).to eq expected_position
    end
  end
end
