class MarsRover
  def self.land(position)
    @position = position
  end

  def self.position
    @position
  end

  def self.perform(command)
    position = @position.split(",")
    
    @position = self.move(command, position)
  end

  def self.move(command, position)
    step = 1
    if command == "M"
      position[1] = position[1].to_i + step
    end
    
    position.join(",")
  end
end
