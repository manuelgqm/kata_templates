require './implementation'

describe 'Tennis game' do
  describe 'defines a Court' do
    let(:num_members) {}

    subject { Tennis.new(num_members) }

    it 'has two players' do
      expect(subject.players_count).to be 2
    end

    context 'when setting the first player scores' do
      it 'has 0 points' do
        expect(subject.points('A')).to be_zero
      end

      it 'has 0 sets' do
        expect(subject.sets('A')).to be_zero
      end

      it 'has 0 games' do
        expect(subject.games('A')).to be_zero
      end
    end

    context 'when setting the second player scores' do
      it 'has 0 points' do
        expect(subject.points('B')).to be_zero
      end

      it 'has 0 sets' do
        expect(subject.sets('B')).to be_zero
      end

      it 'has 0 games' do
        expect(subject.games('B')).to be_zero
      end
    end

    context 'when the player has different number of members' do
      subject { Tennis.new(num_members).num_members }

      context 'has one member each' do
        let(:num_members) { 1 }
        it { is_expected.to be 1 }
      end

      context 'has two members each' do
        let(:num_members) { 2 }
        it { is_expected.to be 2 }
      end
    end
  end

  context 'when playing a game' do
    let(:a_points) { 5 }
    let(:b_points) { 3 }
    let(:expected_winner) { 'A' }
    it('the first player to have at least 4 points in total and a difference greater than 1') do
      expect(subject.winner).to be expected_winner
    end
  end
end
