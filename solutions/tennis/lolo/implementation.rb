class Tennis
  attr_reader :num_members

  def initialize(num_members = 1)
    @players = %w[A B]
    @num_members = num_members
  end

  def players_count
    @players.length
  end

  def points(player)
    0
  end

  def sets(player)
    0
  end

  def games(player)
    0
  end
end
