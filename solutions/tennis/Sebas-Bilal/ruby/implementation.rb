class Match
  attr_accessor :team1, :team2, :points_team1, :points_team2

  def initialize(team1, team2)
    @team1 = team1
    @team2 = team2
    @points_team1 = [0, 0, 0]
    @points_team2 = [0, 0, 0]
  end

  def players_required?
    team1.length == team2.length
  end

  def score(team_number)
    if team_number == 1
      points_team1[0] += 15
    else
      points_team2[0] += 15
    end
  end

  def get_team1_points
    score_conversor(points_team1[0])
  end

  def get_team2_points
    score_conversor(points_team2[0])
  end

  def score_conversor(point)
    case point
    when 0
      'LOVE'
    when 15
      'FIFTEEN'
    when 30
      'THIRTY'
    when 40
      return 'DEUCE' if points_team1[0] == points_team2[0]
      'FORTY'
    end
  end
end