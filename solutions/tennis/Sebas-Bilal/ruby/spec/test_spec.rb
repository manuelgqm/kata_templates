require './implementation'
describe 'Tenis Match' do
  let(:team1) { ['Adrian'] }
  let(:team2) { ['Nadal'] }
  let(:points_team1) { [0, 0, 0] }
  let(:points_team2) { [0, 0, 0] }

  subject! do
    Match.new(team1, team2)
  end

  describe 'starting players for each team' do
    context 'have correct players' do
      it 'validates the players' do
        expect(subject.players_required?).to be_truthy
      end
    end
    context 'have incorrect player, 1 team have 1 more than the other' do
      let(:team2) { %w[Nadal Dani] }

      it 'validates the players' do
        expect(subject.players_required?).to be_falsey
      end
    end
  end

  context 'starting points for each team' do
    it 'starts with 0 points for team 1' do
      expect(subject.points_team1).to eq(points_team1)
    end
    it 'starts with 0 points for team 2' do
      expect(subject.points_team2).to eq(points_team2)
    end
  end

  context 'one team scores' do
    before do
      subject.score(team_scores)
    end

    context 'team 1 scores 1 point' do
      let(:team_scores) { 1 }
      it 'increase score correctly' do
        expect(subject.get_team1_points).to eq('FIFTEEN')
      end
      it 'increase score correctly' do
        expect(subject.get_team2_points).to eq('LOVE')
      end
    end

    context 'team 2 scores 1 point' do
      let(:team_scores) { 2 }
      it 'increase score correctly' do
        expect(subject.get_team1_points).to eq('LOVE')
      end
      it 'increase score correctly' do
        expect(subject.get_team2_points).to eq('FIFTEEN')
      end
    end
  end

  context 'each team scores 1 point' do
    before do
      subject.score(1)
      subject.score(2)
    end

    it 'increase score correctly' do
      expect(subject.get_team1_points).to eq('FIFTEEN')
    end
    it 'increase score correctly' do
      expect(subject.get_team2_points).to eq('FIFTEEN')
    end
  end

  context 'each team scores 40 ' do
    let(:points_team1) { [40, 0, 0] }
    let(:points_team2) { [40, 0, 0] }

    it 'Score in team 1 is equal to team2 and is 40' do
      expect(subject.get_team1_points).to eq('DEUCE')
    end
    it 'Score in team 2 is equal to team1 and is 40' do
      expect(subject.get_team2_points).to eq('DEUCE')
    end
  end
end
