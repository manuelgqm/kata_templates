require 'json'

class Character
  attr_reader :hp

  def initialize(healt_points)
    @hp = healt_points
  end

  def attack(enemy)
    !enemy.instance_of?(self.class)
  end
end

class Player < Character
  attr_reader :armor, :weapons

  @@max_hp = 100
  @@max_armor = 50

  def initialize
    super(@@max_hp)
    @armor = @@max_armor
    @weapons = [Weapon.new('Puños')]
  end
end

class Demon < Character
  attr_reader :name, :attack_points

  def initialize(name = nil, healt_points = nil, attack_points = nil)
    super(healt_points)
    @name = name
    @attack_points = attack_points
  end
end

class Weapon
  attr_reader :name, :capacity, :ammo, :damage

  def initialize(name)
    @name = name
    @capacity = nil
    @ammo = nil
    @damage = nil

    initialize_weapon
  end

  def initialize_weapon
    weapons_json = File.read('./doom/armas.json')
    weapons = JSON.parse(weapons)

    
  end
end
