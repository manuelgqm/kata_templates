require './implementation'
describe Player do
  let(:player) { Player.new }

  context 'when player initilizes' do
    it 'exists' do
      expect(player).to be_truthy
    end

    it 'starts with 100 HP' do
      expect(player.hp).to eq(100)
    end

    it 'starts with 50 Armor' do
      expect(player.armor).to eq(50)
    end

    it 'starts with only one weapon' do
      expect(player.weapons.length).to eq(1)
    end

    it 'starts with fist weapon' do
      expect(player.weapons.first.name).to eq('Puños')
    end
  end

  context 'when player attacks' do
    it 'can attack' do
      expect(player.respond_to?(:attack)).to be_truthy
    end

    it 'can attack a demon' do
      expect(player.attack(Demon.new)).to be_truthy
    end

    it 'cannot attack if is not a demon' do
      expect(player.attack(Player.new)).to be_falsey
    end
  end
end

describe Demon do
  demon_name = 'anyzombi'
  healt_points = 80
  attack_points = 6

  let(:demon) { Demon.new(demon_name, healt_points, attack_points) }

  it 'exists' do
    expect(demon).to be_truthy
  end

  it 'starts with name' do
    expect(demon.name).to eq(demon_name)
  end

  it 'starts with hp ' do
    expect(demon.hp).to eq(healt_points)
  end

  it 'starts with attack points' do
    expect(demon.attack_points).to eq(attack_points)
  end

  it 'can attack' do
    expect(demon.respond_to?(:attack)).to be_truthy
  end

  it 'can attack a player' do
    expect(demon.attack(Player.new)).to be_truthy
  end

  it 'cannot attack if is not a player' do
    expect(demon.attack(Demon.new)).to be_falsey
  end
end

describe Weapon do
  name = 'Puños'
  capacity = "Infinita"
  ammo = "No "
  damage = 15
  let(:weapon) { Weapon.new(name) }

  it 'exists' do
    expect(weapon).to be_truthy
  end

  it 'starts with name' do
    expect(weapon.name).to eq(name)
  end

  it 'starts with capacity' do
    expect(weapon.capacity).to eq(capacity)
  end

  it 'starts with ammo' do
    expect(weapon.ammo).to eq(ammo)
  end

  it 'starts with damage' do
    expect(weapon.damage).to eq(damage)
  end
end
