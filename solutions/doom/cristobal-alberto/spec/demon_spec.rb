require './implementation'

describe Demon do
  describe 'attributes' do
    shared_examples 'has attribute' do |method_name, value|
      describe "##{method_name}" do
        it 'implements the method' do
          expect(Demon.new.respond_to?(method_name)).to be_truthy
        end

        it 'returns the right value' do
          expect(Demon.new(method_name => value).send(method_name)).to eq value
        end
      end
    end

    it_behaves_like 'has attribute', :life, 100
    it_behaves_like 'has attribute', :damage, 50
  end
end
