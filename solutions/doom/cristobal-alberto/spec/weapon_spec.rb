require './implementation'

describe Weapon do
  describe 'attributes' do
    shared_examples 'has attribute' do |method_name, value|
      describe "##{method_name}" do
        it 'implements the method' do
          expect(Weapon.new.respond_to?(method_name)).to be_truthy
        end

        it 'returns the right value' do
          expect(Weapon.new(method_name => value).send(method_name)).to eq value
        end
      end
    end

    it_behaves_like 'has attribute', :name, 'foo'
    it_behaves_like 'has attribute', :ammo, 'foo'
    it_behaves_like 'has attribute', :amount, 'foo'
    it_behaves_like 'has attribute', :damage, 100
    it_behaves_like 'has attribute', :ammo_by_attack, 10
  end

  describe 'methods' do
    describe '#attack' do
      let(:weapon) { Weapon.new(amount: amount, ammo_by_attack: ammo_by_attack) }

      subject! { weapon.attack }

      context 'when the ammo is finite' do
        let(:amount) { '100' }
        let(:ammo_by_attack) { 10 }

        it 'decrease the available ammo' do
          expect(weapon.amount).to eq '90'
        end
      end

      context 'when the ammo is infinite' do
        let(:amount) { described_class::INFINITE_VALUE }
        let(:ammo_by_attack) { 0 }

        it 'still has infinite ammo' do
          expect(weapon.amount).to eq 'Infinita'
        end
      end
    end
  end
end
