require './implementation'

describe Player do
  describe 'attributes' do
    shared_examples 'has attribute' do |method_name, value|
      describe "##{method_name}" do
        it 'implements the method' do
          expect(Player.new.respond_to?(method_name)).to be_truthy
        end

        it 'returns the right value' do
          expect(Player.new(method_name => value).send(method_name)).to eq value
        end
      end
    end

    it_behaves_like 'has attribute', :life, 100
    it_behaves_like 'has attribute', :armor, 50
    it_behaves_like 'has attribute', :weapons, ['Puños']
  end
end
