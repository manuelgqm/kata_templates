class Player
  attr_accessor :life, :armor, :weapons

  def initialize(life: 100, armor: 50, weapons: ['Puños'])
    @life = life
    @armor = armor
    @weapons = weapons
  end
end
