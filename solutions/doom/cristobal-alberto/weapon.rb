class Weapon
  attr_accessor :name, :ammo, :amount, :damage, :ammo_by_attack

  INFINITE_VALUE = "Infinita"

  def initialize(name: nil, ammo: nil, amount: nil, damage: nil, ammo_by_attack: nil)
    @name = name
    @ammo = ammo
    @amount = amount
    @damage = damage
    @ammo_by_attack = ammo_by_attack
  end

  def attack
    return if amount == INFINITE_VALUE

    @amount = (amount.to_i - ammo_by_attack).to_s
  end
end
