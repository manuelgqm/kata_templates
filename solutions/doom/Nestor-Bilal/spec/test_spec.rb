require './implementation'
describe 'Player' do
  subject! do 
    Player.new
  end

  it 'starts with 100 life points' do
    expect(subject.life).to eq(100)
  end

  it 'starts with 50 armor points' do
    expect(subject.armor).to eq(50)
  end
  
  context "when gains life" do
    before do
      subject.set_life(starting_life)
      subject.gain_life(life_increased)
    end    
    let(:life_increased) { 10 }
    let(:starting_life) { 50 }
    
    it 'while his life its lower than max life' do
      expect(subject.life).to eq(60)
    end

    context 'doesnt increase past maximun life' do
      let(:starting_life) { 92 }
      
      it '' do        
        expect(subject.life).to eq(100)
      end
    end
  end

  context "when gains armor" do
    before do
      subject.set_armor(starting_armor)
      subject.gain_armor(armor_increased)
    end    
    let(:armor_increased) { 10 }
    let(:starting_armor) { 40 }
    
    it 'while his armor its lower than max armor' do
      expect(subject.armor).to eq(50)
    end

    context 'doesnt increase past maximun armor' do
      let(:starting_armor) { 45 }
      
      it '' do        
        expect(subject.armor).to eq(50)
      end
    end
  end
end

describe 'Weapons' do
  subject do
    Weapon.new(name, ammo_type, capacity, damage)
  end
  let(:name) {'Puños'}
  let(:ammo_type) {'no'}
  let(:capacity) {'Infinita'}
  let(:damage) { '15' }

  before do 
    subject.perform_attack()
  end

  context 'when weapon has capacity' do
    it 'decreases ammo' do
      
    end
  end

  context 'when weapon doesnt have capacity' do

  end
  
end
