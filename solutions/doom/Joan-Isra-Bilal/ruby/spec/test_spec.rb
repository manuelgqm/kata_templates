require './implementation'
describe 'Entity' do
  let(:entity) { Entity.new }

  describe 'when we initizliced Entity with 100 life points' do
    before do 
      entity.life = 100
    end

    it 'has 100 life points' do
      expect(entity.life).to eq(100)
    end

    context 'we receive' do
      context '30 damage points' do
        before do
          entity.receive_damage(30)
        end
        it 'stays with 80 life points' do
          expect(entity.life).to eq(70)
        end
      end
    end
  end
end

describe Player do
  let(:player) { Player.new }

  describe 'when player is initilized' do
    it "inherits from Entity class" do
      expect(player).to be_a(Entity)
    end
  end
end