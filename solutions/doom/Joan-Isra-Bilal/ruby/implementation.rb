class Entity
  attr_accessor :life

  def receive_damage(dmg)
    self.life = self.life - dmg
  end
end

class Player < Entity
  
end