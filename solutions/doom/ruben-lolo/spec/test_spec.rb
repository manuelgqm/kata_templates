require './implementation'
describe 'Player' do
  it 'has a maximun health value of 100' do
    expect(Player.max_health).to be 100
  end

  it 'has a maximun armour value of 50' do
    expect(Player.max_armour).to be 50
  end

  it 'has alwais punch' do
    expect(Player.weapons).to include :punch
  end

  it 'has a maximun weapons of 5' do
    expect(Player.max_weapons).to be 5
  end

  it 'can pick new weapons' do
    expect(Player.pick_weapon(:chainsaw)).to change {Player.weapons}.from([:punch]).to([:punch, :chainsaw])
  end

end

