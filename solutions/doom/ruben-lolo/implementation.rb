class Player
  attr_reader :weapons
  
  def self.max_health
    100
  end

  def self.max_armour
    50
  end

  def self.weapons
    [:punch]
  end

  def self.max_weapons
    5
  end

  def self.pick_weapon(weapon)
    @weapons.push(weapon)
  end
end
