class Player
  attr_accessor :life, :armour, :weapons

  def initialize
    @life = 100
    @armour = 50
    @weapons = [ Weapon.new, nil, nil, nil, nil ]
  end
end

class Weapon
  attr_accessor :name, :ammunition, :capacity, :shot, :damage

  def initialize
    @name = 'Fist'
    @ammunition = 0
    @capacity = -1
    @shot = 0
    @damage = 15
  end
end