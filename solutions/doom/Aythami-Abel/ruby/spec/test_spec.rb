require './implementation'
describe 'DOOM' do
  describe '#Player' do
    subject!(:initial_player) { Player.new }
    shared_examples 'player conditions' do
      it 'has a maximum of 100 life points' do
        # Validamos que el jugador nunca se pase de los 100
        expect(initial_player.life).to be <= 100
      end
      it 'has a maximum of 50 armour points' do
        expect(initial_player.armour).to be <= 50
      end
    end
    describe 'with initial requirements' do
      let(:initial_life_points){100}
      let(:initial_armour_points){50}
      it 'starts with 100 life points' do
        # Asumimos que siempre empezará con 100
        expect(initial_player.life).to equal initial_life_points
      end
      it 'starts with 50 armour points' do
        expect(initial_player.armour).to equal initial_armour_points
      end
      it 'starts with fists as default weapon' do
        expect(initial_player.weapons[0]).to have_attributes(name: 'Fist')
      end
      it 'starts with a capacity of 5 weapons' do
        expect(initial_player.weapons.length).to equal 5
      end
    end
  end
  describe '#Weapon' do
    # it 'failing test' do
    #   expect(Kata.example).to be_truthy
    # end
  end
  describe '#Game' do
    # it 'failing test' do
    #   expect(Kata.example).to be_truthy
    # end
  end
end
