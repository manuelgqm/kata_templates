require './implementation'

RSpec.shared_examples 'common variables' do
  context 'are asigned' do
    it "have name" do
      expect(subject.name).to eq(name)
    end
    
    it "have damage" do
      expect(subject.damage).to eq(damage)
    end
  end
end

RSpec.shared_examples 'common functions' do
  it 'responds to perform attack' do
    expect(subject.respond_to? :perform_attack).to be_truthy
  end
end

describe 'Player' do
  subject! do
    Player.new
  end

  it 'starts with 100 life points' do
    expect(subject.life).to eq(100)
  end

  it 'starts with 50 armor points' do
    expect(subject.armor).to eq(50)
  end

  context "when gains life" do
    before do
      subject.set_life(starting_life)
      subject.gain_life(life_increased)
    end
    let(:life_increased) { 10 }
    let(:starting_life) { 50 }

    it 'while his life its lower than max life' do
      expect(subject.life).to eq(60)
    end

    context 'doesnt increase past maximun life' do
      let(:starting_life) { 92 }

      it '' do
        expect(subject.life).to eq(100)
      end
    end
  end

  context "when gains armor" do
    before do
      subject.set_armor(starting_armor)
      subject.gain_armor(armor_increased)
    end
    let(:armor_increased) { 10 }
    let(:starting_armor) { 40 }

    it 'while his armor its lower than max armor' do
      expect(subject.armor).to eq(50)
    end

    context 'doesnt increase past maximun armor' do
      let(:starting_armor) { 45 }

      it '' do
        expect(subject.armor).to eq(50)
      end
    end
  end
end

describe 'Weapons' do
  subject do
    Weapon.new(name, damage)
  end
  let(:name) {'Puños'}
  let(:damage) { 15 }

  include_examples 'common variables'
  include_examples 'common functions'
end

describe "RangedWeapons" do
  subject do
    RangedWeapons.new(name, damage, capacity, bullet_per_shot)
  end
  let(:name) {'Puños'}
  let(:damage) { 15 }
  let(:capacity) { 10 }
  let(:bullet_per_shot) { 2 }

  include_examples 'common variables'
  include_examples 'common functions'

  it 'has capacity' do
    expect(subject.capacity).to eq(capacity)
  end

  it 'has capacity' do
    expect(subject.bullet_per_shot).to eq(bullet_per_shot)
  end

  describe "#perform_attack" do
    before do
      subject.perform_attack
    end

    it "when attack the capacity decrease" do
      expect(subject.capacity).to eq 8
    end
  end
end