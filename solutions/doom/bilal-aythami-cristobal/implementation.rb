class Player
  attr_accessor :life, :armor, :weapons

  def initialize()
    self.life = 100
    self.armor = 50
  end

  def set_life(new_life)
    self.life = new_life
  end

  def set_armor(new_armor)
    self.armor = new_armor
  end

  def gain_life(life_gained)
    self.life += life_gained
    self.life = 100 if life > 100
  end

  def gain_armor(armor_gained)
    self.armor += armor_gained
    self.armor = 50 if armor > 50
  end

  def lose_life(life_lost)
    self.life -= life_lost
  end

  def lose_armor(armor_lost)
    self.armor -= armor_lost
  end
end

class Weapon
  attr_accessor :name, :damage

  def initialize(name, damage)
    self.name = name
    self.damage = damage
  end

  def perform_attack
  end
end

class RangedWeapons < Weapon
  attr_accessor :capacity, :bullet_per_shot
  def initialize(name, damage, capacity, bullet_per_shot)
    super(name, damage)
    self.capacity = capacity
    self.bullet_per_shot = bullet_per_shot
  end

  def perform_attack
    self.capacity -= self.bullet_per_shot
  end
end