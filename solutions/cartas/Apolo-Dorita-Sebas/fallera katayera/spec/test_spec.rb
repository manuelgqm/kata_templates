require './implementation'

describe 'Fallera katallera' do
  let(:activeHand) {[0,0,0,0,0,0,0]}
  let(:passiveHand) {[0,0,0,0,0,0,0]}
  subject! do
    Fallera.new(activeHand,passiveHand)
  end
    
  context 'when game starts' do
    expected_number_of_cards = 7
    it 'active player has 7 cards' do
      expect(subject.active_length).to eq expected_number_of_cards
    end

    #it 'passive player has 7 cards' do
     # expect(subject.pasiveHand.length).to eq expected_number_of_cards
    #end
  end
end
