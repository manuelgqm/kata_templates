require './implementation'
describe 'FalleraKatayera' do
  num_players = 2
  subject(:fallera) { FalleraKatayera.new(num_players) }
  it 'starts with n number of players' do
    expect(fallera.num_players).to eq num_players
  end

  context 'each player starts with 7 cards' do
    num_cards_expected = 7
    it 'player 1 has 7 cards' do
      expect(fallera.players[0].length).to eq num_cards_expected
    end
    it 'player 2 has 7 cards' do
      expect(fallera.players[1].length).to eq num_cards_expected
    end
    context 'when have start winner' do
      before do
        stub = instance_double(FalleraKatayera)
        allow(stub).to receive(:player_hand) do
          [
            { type: 'I', number: 2 },
            { type: 'I', number: 3 },
            { type: 'I', number: 4 },
            { type: 'I', number: 5 },
            { type: 'I', number: 7 },
            { type: 'M', number: 4 },
            { type: 'M', number: 10 }
          ]
        end
      end
      let(:player_hand) do
      end
      it 'has a winner' do
        expect(fallera.winner).to eq true
      end
    end
    context 'when have not start winner' do
      let(:player_hand) do
        [
          { type: 'I', number: 2 },
          { type: 'I', number: 3 },
          { type: 'I', number: 4 },
          { type: 'I', number: 5 },
          { type: 'M', number: 7 },
          { type: 'M', number: 4 },
          { type: 'M', number: 10 }
        ]
      end
      it 'has or not a winner' do
        expect(fallera.winner).to eq false
      end
    end

  end

  context 'cards' do
    it 'has type key' do
      expect(fallera.players[0][0].keys.include?(:type)).to be_truthy
    end

    it 'has number key' do
      expect(fallera.players[0][0].keys.include?(:number)).to be_truthy
    end
  end
end
