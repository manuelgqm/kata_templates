class FalleraKatayera
  attr_reader :num_players, :players

  def initialize(num_players)
    @num_players = num_players
    @card_types = { '0': 'I', '1': 'M' }
    @players = []
    (1..num_players).each do
      @players.push(player_hand)
    end
  end

  def player_hand
    cards = []
    7.times do
      cards << { type: @card_types[rand(0..1).to_s.to_sym], number: rand(0..10) }
    end
    cards
  end

  def winner
    @players.each do |player_cards|
      result = player_cards.select { |element| element[:type] == 'I' }
      return false if result.uniq.length < 5
    end
    true
  end
end
