require './implementation'
describe 'Card' do

  subject { Card.new(kind_of: kind)}
  let(:kind) { 'loquesea' }

  it 'has a kind_of attribute' do
    expect(subject.kind_of).to be_truthy
  end

  context 'when the card is a monster' do
    subject { Card.new(kind_of: kind, attack_points: attack_points)}

    let(:kind) { 'monster' }
    let(:attack_points) { 8 }
    it 'has a monster kind' do
      expect(subject.kind_of).to be(:monster)
    end

    it 'has attack points to attack other players monsters' do
      expect(subject.attack_points).to be_truthy
    end

    it 'has numeric attack points' do
      expect(subject.attack_points).to be_an(Integer)
    end
  end

  context 'when the card is an ingredient' do
    subject { Card.new(kind_of: kind, the_ingredient: ingredient_of) }
    let(:kind) { 'ingredient' }
    let(:ingredient_of) { 'cebolla' }

    it 'has a ingredient kind' do
      expect(subject.kind_of).to be(kind.to_sym)
    end

    it 'has an ingrendient identifier (name)' do
      expect(subject.ingredient).to be(ingredient_of)
    end
  end
end

describe 'Deck' do
  subject { Deck.new(cards_number) }
  let (:cards_number) { 8 }

  it('has an array of cards') do
    expect(subject.cards).to be_truthy
  end

  it('contains n cards') do
    expect(subject.cards.length).to be(cards_number)
  end
end
