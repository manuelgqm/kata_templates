class Card

  attr_reader :kind_of
  attr_reader :attack_points
  attr_reader :ingredient

  def initialize(kind_of: nil, attack_points: nil, the_ingredient: '')
    @kind_of = kind_of.to_sym
    @attack_points = attack_points.to_i if is_monster? && attack_points
    @ingredient = the_ingredient if is_ingredient? && the_ingredient
  end

  def is_monster?
    @kind_of == :monster
  end

  def is_ingredient?
    @kind_of == :ingredient
  end

end

class Deck
  attr_reader :deck

  def initialize(cards_number) do
    @cards = []
    card_number.times { @deck << Card.new() }
  end

end