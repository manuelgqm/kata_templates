# frozen_string_literal: true

class Card
  attr_reader :value

  def initialize(type = :ingredient, value = 'pollo')
    @type = type
    @value = value
  end

  def ingredient?
    @type == :ingredient
  end

  def monster?
    @type == :monster
  end
end

class Player
  attr_reader :hand

  def initialize
    @hand = []
  end

  def add_to_hand(card)
    @hand.push card
  end
end
