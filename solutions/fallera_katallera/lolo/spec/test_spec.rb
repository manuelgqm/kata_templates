# frozen_string_literal: true

require './implementation'

describe 'A card' do
  context 'of ingredient type' do
    subject { Card.new }

    it 'is a ingredient' do
      expect(subject.ingredient?).to be_truthy
    end

    it 'is not a monster' do
      expect(subject.monster?).to be_falsy
    end

    it 'contains one ingredient of the list' do
      ingredients = %w[pollo conejo tomate alcachofa habas bajoqueta]
      expect(ingredients).to include subject.value
    end
  end

  context 'of monster type' do
    subject { Card.new :monster, 10 }

    it 'is a monster' do
      expect(subject.monster?).to be_truthy
    end

    it 'is not a ingredient' do
      expect(subject.ingredient?).to be_falsy
    end

    it 'contains the monster attack points' do
      expect(subject.value).to be 10
    end
  end
end

describe 'A player' do
  subject { Player.new }

  it 'can add a monster to his hand' do
    card = Card.new :monster
    subject.add_to_hand(card)

    expect(subject.hand.first.monster?).to be true
  end

  it 'can add a ingredient to his hand' do
    card = Card.new :ingredient
    subject.add_to_hand(card)

    expect(subject.hand.first.ingredient?).to be_truthy
  end
end
