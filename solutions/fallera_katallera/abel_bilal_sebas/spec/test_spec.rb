require './implementation'
describe 'Katayera' do
  describe 'Kata' do

  end

  describe 'Cards' do
    context 'without params' do
      subject!(:card) { Card.new }

      it 'has type defined by default' do
        expect(card.type).not_to be_nil
      end
      it 'has value defined by default' do
        expect(card.value).not_to be_nil
      end
    end
    shared_examples 'has attributes' do
      it 'has type defined' do
        expect(card.type).to eq(type)
      end
      it 'has value defined ' do
        expect(card.value).to eq(value)
      end
    end
    context 'with params' do
      subject!(:card) { Card.new(type: type, value: value) }
      context 'when is monster' do
        let(:value) { 2 }
        let(:type) { :monster }

        include_examples 'has attributes'
      end
      context 'when is ingredient' do
        let(:value) { 3 }
        let(:type) { :ingredient }

        include_examples 'has attributes'
      end
    end
  end
end
