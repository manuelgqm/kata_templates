class Player
  attr_accessor :hand
  def initialize
    self.hand = []
    (0..6).each{
      self.hand << rand(0..20)
    }
  end

  def hand_size
    self.hand.length
  end

  def is_winner?
    winner_hand = [1, 2, 3, 4, 5]
    result = winner_hand - self.hand
    if result.empty?
      true
    else
      false
    end
  end
end

class Card
  attr_accessor :type, :value
  def initialize(type)
    self.type = type
    if type == 'Ingredient'
      self.value = 'Pollo'
    end
  end
end