require './implementation'
describe 'Fallera Katallera' do

  let(:player_1) { Player.new() }
  let(:card) { Card.new(type) }
  

  describe 'Player' do
    it 'starts with a hand of 7 cards' do
      expect(player_1.hand_size).to eq(7)
    end

    describe 'his hand' do
      before do
        allow(player_1).to receive(:hand).and_return(hand)
      end
      context 'is winner' do
        let(:hand) { [1, 2, 3, 4, 5, 6, 7] }      
        it '' do
          expect(player_1.is_winner?).to be_truthy
        end
      end
      context 'is not winner' do
        let(:hand) { [1, 8, 3, 3, 6, 6, 7] }      
        it '' do
          expect(player_1.is_winner?).to be_falsy
        end
      end
    end
  end

  describe 'Cards' do
    before do
      allow(card).to receive(:value).and_return(value)
    end
    
    context 'is ingredient card' do
      let(:type) { 'Ingredient' }
      let(:value) { 'Pollo' }
      
      it '' do
        expect(card.type).to eq('Ingredient')
      end

      it 'self value is "Pollo"' do
        expect(card.value).to eq('Pollo')
      end
    end

    context 'is monster card' do
      let(:type) { 'Monster' }

      it '' do
        expect(card.type).to eq('Monster')
      end
    end
  end
end