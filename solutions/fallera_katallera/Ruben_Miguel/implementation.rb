class Card
  def initialize(card_type)
    @card_kind = card_type
  end

  def kind
    @card_kind
  end
end

class Ingredient < Card
  def initialize(ingredient_type)
    super("Ingrediente")
    @ingredient_kind = ingredient_type
  end
  
  def ingredient_name
    @ingredient_kind
  end  
end

class Monster < Card
  def initialize(attack_number)
    super("Monstruo")
    @attack_power = attack_number
  end
  
  def attack_points
    @attack_power
  end
end

class Player
  def cards_in_hand
    return 7
  end

  def initialize(card_array = nil)
    unless card_array.nil? 
      @card_hand = card_array
    else
      @card_hand = []
      (1..5).each { |x|
        @card_hand.append(Ingredient.new("ingredient_#{x}"))
      }
      (1..2).each { |x|
        @card_hand.append(Monster.new(x*2))
      }
    end
  end

  def has_five_ingredients?
    num_ingredients = 0
    return true
  end
end