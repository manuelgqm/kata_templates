require './implementation'
describe 'Card' do

  context 'when the card is a generic card' do
    subject { Card.new(expected_kind) }

    let(:expected_kind) { "Ingrediente" }

    it 'must have a kind' do
      expect(subject.kind).not_to be_nil
    end
  end

  context 'when the card is an ingredient' do
    subject { Ingredient.new(expected_ingredient) }
    
    let(:expected_ingredient) { "Tomate" }

    it 'must be of kind Ingrediente' do
      expect(subject.kind).to be == "Ingrediente"
    end

    it 'has the ingredient name' do
      expect(subject.ingredient_name).to be == expected_ingredient
    end
  end

  context 'when the card is a monster' do
    subject { Monster.new(expected_attack_points) }
    
    let(:expected_attack_points) { 15 }

    it 'must be of kind Monstruo' do
      expect(subject.kind).to be == "Monstruo"
    end

    it 'has a monsters attack' do
      expect(subject.attack_points).to be == expected_attack_points
    end
  end
end

describe 'Player' do
  context 'when the player start to play' do
    it 'has seven cards' do
      expect(Player.new().cards_in_hand).to equal(7)
    end
  end

  context 'when the player has five different ingredients' do
    subject {
      Player.new([
        ingredient_1,
        ingredient_2,
        ingredient_3,
        ingredient_4,
        ingredient_5,
        monster_1,
        monster_2
      ])
    }

    let(:ingredient_1) { Ingredient.new("ingredient_1") }
    let(:ingredient_2) { Ingredient.new("ingredient_2") }
    let(:ingredient_3) { Ingredient.new("ingredient_3") }
    let(:ingredient_4) { Ingredient.new("ingredient_4") }
    let(:ingredient_5) { Ingredient.new("ingredient_5") }
    let(:monster_1) { Monster.new(3) }
    let(:monster_2) { Monster.new(5) }
    
    it 'is the winner' do
      expect(subject.has_five_ingredients?).to be_truthy
    end
  end

  context 'when the player hasn\'t yet the five different ingredients' do
    subject {
      Player.new([
        ingredient_1,
        ingredient_2,
        monster_1,
        monster_2,
        monster_3,
        monster_4,
        monster_5
      ])
    }

    let(:ingredient_1) { Ingredient.new("ingredient_1") }
    let(:ingredient_2) { Ingredient.new("ingredient_2") }
    let(:monster_1) { Monster.new(3) }
    let(:monster_2) { Monster.new(5) }
    let(:monster_3) { Monster.new(5) }
    let(:monster_4) { Monster.new(5) }
    let(:monster_5) { Monster.new(5) }
    
    it 'is not the winner' do
      expect(subject.has_five_ingredients?).to be_falsey
    end
  end
end

