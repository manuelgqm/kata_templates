class Deck

  def initialize(number_of_cards)
    @cards = (1..number_of_cards).map{ rand(2) > 1 ? Card.monster : Card.ingredient }
  end

  def take
    @cards.pop
  end

end
