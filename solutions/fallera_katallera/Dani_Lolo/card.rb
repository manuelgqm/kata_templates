class Card

  attr_reader :attack_value

  def initialize(kind_of)
    @kind_of = kind_of.to_sym
    @attack_value = (rand(10)+1) if monster?
  end

  def monster?
    @kind_of == :monster
  end

  def ingredient?
    @kind_of == :ingredient
  end

  def self.monster
    Card.new(:monster)
  end

  def self.ingredient
    Card.new(:ingredient)
  end
end
