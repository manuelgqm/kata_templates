class Player

  attr_reader :cards

  def initialize(deck)
    @deck=deck
    @cards=7.times.map{ deck.take }
  end

  def pick
    @cards << @deck.take
  end

  def winner?
    @cards.select{|card| card.ingredient? }.size >= 5
  end

end
