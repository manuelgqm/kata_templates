require './implementation'
describe 'Fallera Katallera' do
  let(:number_of_cards) { 50 }

  describe 'Player' do
      let(:deck) { Deck.new(number_of_cards) }
      let(:player) { Player.new(deck) }

      it 'starts with seven cards' do
          expect(player.cards.size).to eq 7
      end

      context 'in a game' do
        it 'obtains a new card when picks a card' do
          expect{ player.pick }.to change{ player.cards.size }.by(1)
        end

      end

      it 'cannot bet with ingredient'
  end

  describe 'Card' do
    let(:monster) { Card.monster }
    let(:ingredient) { Card.ingredient }

    it { expect(monster).to be_monster }
    it { expect(ingredient).to be_ingredient }
    it 'has attack_value when card is a monster' do
      expect(monster.attack_value).to be_an(Integer)
    end
    it 'has a non zero attack_value when card is a monster' do
      expect(monster.attack_value).to be > 0
    end
    it 'has not value when card is ingredient' do
      expect(ingredient.attack_value).to be_nil
    end
  end

  describe 'Deck' do
    let(:deck) { Deck.new(number_of_cards) }
    it 'obtains a card when taken from deck' do
       expect(deck.take).to be_kind_of(Card)
    end   
  end

end
