class Player
  attr_accessor :hand

  def initialize
    @hand = []
    (0..6).each{
      @hand << rand(0..20)
    }
  end

  def hand_size
    @hand.size
  end

  def is_winner?
    winner_condition = [1,2,3,4,5]
    
    if self.hand & winner_condition == winner_condition
      true
    else
      false
    end
  end
end
