require './implementation'
describe 'Fallera Katallera' do
  subject! do
    #player_1.draw_first_hand
  end

  let(:player_1) { Player.new() }
  let(:winner_hand) { [1,2,3,4,5,6,7] }
  
  describe 'Initial game' do
    before do
      allow(player_1).to receive(:hand).and_return(winner_hand)
    end
    
    let(:cards_per_player){ 7 }
    
    it 'each player has 7 cards in hand' do
      expect(player_1.hand_size).to eq(cards_per_player)
    end

    it 'player has winner hand' do
      expect(player_1.is_winner?).to be_truthy
    end

  end

  # Comprobar mas de 1 player

  # comprobar condicion de victoria

  # Comprobar ingredientes [1,2,3,4,5]


end