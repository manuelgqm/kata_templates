require 'byebug'

# Service kata called "The Greeting"
class TheGreeting

  GREETING = 'Hello'

  def greet(name)
    "#{greet_initial(name)}#{greet_body(name)}"
  end

  private

  def greet_initial(name)
    return "#{GREETING.upcase} " if name && !name.is_a?(Array) && is_upcase?(name)

    "#{GREETING}, "
  end

  def greet_body(name)
    return greeting_more_one_name(name) if input_is_array?(name)
    return greeting_default if name.nil?
    return greeting_name_in_upcase(name) if is_upcase?(name)

    greeting_unique_name(name)
  end

  def greeting_more_one_name(array)
    if there_more_than_one_name_in_quotes?(array)
      separated_by_comma = separate_by_comma(array)
      greet_more_names(separated_by_comma)
    elsif array.count == 2
      greet_two_names(array)
    else
      greet_more_names(array)
    end
  end

  def separate_by_comma(array)
    array.map { |name|
      name.split.size == 1 ? name : name.split(',').map(&:strip)
    }.flatten
  end

  def there_more_than_one_name_in_quotes?(array)
    array.any? { |name| name.split.size > 1 }
  end

  def greeting_name_in_upcase(string)
    "#{string}!"
  end

  def input_is_array?(array)
    array.is_a?(Array)
  end

  def greet_two_names(array)
    "#{array[0]} and #{array[1]}."
  end

  def greet_more_names(array)
    if check_if_any_name_is_uppercase(array)
      return "#{greet_more_names(list_of_names_in_downcase(array))} AND " \
             "#{GREETING.upcase} #{list_of_names_in_uppercase(array).first}!"
    end

    greet = array[0..-2].join(', ')
    greet << ", and #{array.last}."
  end

  def list_of_names_in_downcase(array)
    array.reject { |name| name == name.upcase }
  end

  def list_of_names_in_uppercase(array)
    array.select { |name| name == name.upcase }
  end

  def check_if_any_name_is_uppercase(array)
    array.any? { |item| item == item.upcase }
  end

  def greeting_unique_name(string)
    "#{string}."
  end

  def greeting_default
    'my friend.'
  end

  def is_upcase?(string)
    string == string.upcase
  end
end
