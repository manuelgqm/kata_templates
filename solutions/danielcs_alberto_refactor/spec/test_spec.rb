# frozen_string_literal: true

require './implementation'

describe TheGreeting do
  subject do
    TheGreeting.new
  end

  describe '#greet' do
    context 'when a given a one name' do
      it 'should return interpolated string' do
        # Arrange
        one_name = 'Bob'
        expected_result = "Hello, #{one_name}."
        # Act
        result = subject.greet(one_name)
        # Assert
        expect(result).to eq expected_result
      end
    end
    context 'when name is null' do
      it 'should return default greet string' do
        # Arrange
        no_name = nil
        expected_result = 'Hello, my friend.'
        # Act
        result = subject.greet(no_name)
        # Assert
        expect(result).to eq expected_result
      end
    end
    context 'when name is uppercase' do
      it 'should return an uppercase greeting' do
        # Arrange
        name_upcase = 'JERRY'
        expected_result = "HELLO #{name_upcase}!"
        # Act
        result = subject.greet(name_upcase)
        # Assert
        expect(result).to eq expected_result
      end
    end
    context 'when is an array of two names' do
      it 'should print both names' do
        # Arrange
        two_names = %w[Jill Jane]
        expected_result = "Hello, #{two_names[0]} and #{two_names[1]}."
        # Act
        result = subject.greet(two_names)
        # Assert
        expect(result).to eq expected_result
      end
    end
    context 'when is an array more than two names' do
      it 'should print the last name after "and"' do
        # Arrange
        array_names_three = %w[Amy Brian Charlotte]
        expected_result_three_names = "Hello, #{array_names_three[0]}, #{array_names_three[1]}, and #{array_names_three[2]}."
        # Act
        result_three_names = subject.greet(array_names_three)
        # Assert
        expect(expected_result_three_names).to eq result_three_names

        # Arrange
        array_names_four = %w[Amy Brian Charlotte Juan]
        expected_result_four_names = "Hello, #{array_names_four[0]}, #{array_names_four[1]}, #{array_names_four[2]}, and #{array_names_four[3]}."
        # Act
        result_four_names = subject.greet(array_names_four)
        # Assert
        expect(expected_result_four_names).to eq result_four_names

        # Arrange
        array_names_five = %w[Amy Brian Charlotte Juan Marian]
        expected_result_five_names = "Hello, #{array_names_five[0]}, #{array_names_five[1]}, #{array_names_five[2]}, #{array_names_five[3]}, and #{array_names_five[4]}."
        # Act
        result_five_names = subject.greet(array_names_five)
        # Assert
        expect(expected_result_five_names).to eq result_five_names
      end
    end
    context 'when allow mixing of normal and shouted, separate them with commas' do
      it 'should print shouted names by separating the response into two greetings' do
        # Arrange
        names_three_shouted_one = %w[AMY Brian Charlotte]
        expected_result_three_names = "Hello, #{names_three_shouted_one[1]}, and #{names_three_shouted_one[2]}. AND HELLO #{names_three_shouted_one[0]}!"
        # Act
        result_three_names = subject.greet(names_three_shouted_one)
        # Assert
        expect(expected_result_three_names).to eq result_three_names

        # Arrange
        names_three_shouted_two = %w[Amy BRIAN Charlotte]
        expected_result_four_names = "Hello, #{names_three_shouted_two[0]}, and #{names_three_shouted_two[2]}. AND HELLO #{names_three_shouted_two[1]}!"
        # Act
        result_four_names = subject.greet(names_three_shouted_two)
        # Assert
        expect(expected_result_four_names).to eq result_four_names

        # Arrange
        names_three_shouted_three = %w[Amy Brian CHARLOTTE]
        expected_result_five_names = "Hello, #{names_three_shouted_three[0]}, and #{names_three_shouted_three[1]}. AND HELLO #{names_three_shouted_three[2]}!"
        # Act
        result_five_names = subject.greet(names_three_shouted_three)
        # Assert
        expect(expected_result_five_names).to eq result_five_names

        # Arrange
        names_three_shouted_three = %w[Amy Brian CHARLOTTE PEPE]
        expected_result_five_names = "Hello, #{names_three_shouted_three[0]}, and #{names_three_shouted_three[1]}. AND HELLO #{names_three_shouted_three[2]} AND #{names_three_shouted_three[3]}!"
        # Act
        result_five_names = subject.greet(names_three_shouted_three)
        # Assert
        # expect(expected_result_five_names).to eq result_five_names
        expect(result_five_names).to eq expected_result_five_names
      end
    end
    context 'when any entries in name are a string containing a comma' do
      it 'should print amount of names from array' do
        # Arrange
        expected_result = 'Hello, Bob, Charlie, and Dianne.'
        array_name = ['Bob', 'Charlie, Dianne']
        # Act
        result = subject.greet(array_name)
        # Assert
        expect(result).to eq expected_result
      end
    end
    context 'when more than one entry in name is a string containing a comma' do
      it 'should print amount of names from array' do
        # Arrange
        expected_result = 'Hello, Bob, Charlie, Dianne, Paco, and Pepe.'
        array_name = ['Bob', 'Charlie, Dianne', 'Paco, Pepe']
        # Act
        result = subject.greet(array_name)
        # Assert
        expect(result).to eq expected_result
      end
    end
  end
end
