require './implementation'
describe 'A String Calculator' do
  it 'returns 0 when empty string is passed' do
    expect(calculate("")).to eq 0
  end

  it 'returns 1 when string "1" is passed' do
    expect(calculate("1")).to eq 1
  end

  it 'returns 3 when string "1,2" is passed' do
    expect(calculate("1,2")).to eq 3
  end
  
  it 'returns 5 when string "1,2,2" is passed' do
    expect(calculate("1,2,2")).to eq 5
  end

  context "can use new line character as separator" do
    it 'returns 6 when string "1\n2,3" is passed' do
      expect(calculate("1\n2,3")).to eq 6
    end

    it 'returns 6 when string "1\n2\n3" is passed' do
      expect(calculate("1\n2\n3")).to eq 6
    end
  end

  context "can support custom delimiter" do
    it 'return 3 when string "//;1;2" is passed' do
      expect(calculate("//;\n1;2")).to eq 3
    end
  end
end
