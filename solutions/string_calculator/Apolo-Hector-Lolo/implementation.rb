def calculate(string_number)
  delimiter = ","
  if string_number.start_with?("//")
    delimiter = string_number[2]
    string_number = string_number[4..]
  end
  normalized_string_number = string_number.gsub("\n", delimiter)
  string_numbers = normalized_string_number.split(delimiter)
  string_numbers.map(&:to_i).sum
end