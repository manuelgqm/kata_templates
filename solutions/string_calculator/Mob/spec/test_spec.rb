require './implementation'

describe 'String calculator' do
  it 'returns 0 when input is empty string' do
    empty_string_numbers = "" # Arrange
    expected = 0

    actual = StringCalculator.calculate(empty_string_numbers) # Act
    
    expect(actual).to eq(expected) # Assert
  end

  it 'returns 1 when input is one string' do
    single_string_number = "1"
    expected = 1

    expect(StringCalculator.calculate(single_string_number)).to eq(expected)
  end
  
  it 'returns 2 when input is two string' do
    single_string_number = "2"
    expected = 2
    
    expect(StringCalculator.calculate(single_string_number)).to eq(expected)
  end

  it 'returns 3 when input is "1,2" string' do
    string_numbers = "1,2"
    expected = 3
    
    expect(StringCalculator.calculate(string_numbers)).to eq(expected)
  end
end
