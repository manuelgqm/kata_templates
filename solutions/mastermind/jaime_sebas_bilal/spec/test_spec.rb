require './implementation'
describe 'Mastermind' do
  # it 'returns [1,0] if values are the same' do
  #   expect(Mastermind.evaluate([1], [1])).to eq [1, 0]
  # end
  # it 'returns [0,0] if all values are incorrect' do
  #   expect(Mastermind.evaluate([1], [2])).to eq [0, 0]
  # end
  subject { Mastermind.evaluate(secret_arr, attempt_arr) }

  let(:secret_arr) { [1] }
  let(:attempt_arr) { [1] }

  context 'when attempt and secret are the same' do
    let(:secret_arr) { [1, 2] }
    let(:attempt_arr) { [1, 2] }

    it 'returns [2,0]' do
      expected_response = [2, 0]
      expect(subject).to eq expected_response
    end
  end
  context 'when attempt and secret are totally different' do
    let(:secret_arr) { [1, 1] }
    let(:attempt_arr) { [2, 2] }

    it 'returns [0,0]' do
      expected_response = [0, 0]
      expect(subject).to eq expected_response
    end
  end

  context 'when attempt have values on different positions' do
    let(:secret_arr) { [1, 2] }
    let(:attempt_arr) { [3, 1] }

    it 'returns [0, 1]' do
      expected_response = [0, 1]
      expect(subject).to eq expected_response
    end
  end

  # context 'when attempt have values on different and exact positions ' do
  #   let(:secret_arr) { [1, 2, 1] }
  #   let(:attempt_arr) { [3, 1, 1] }

  #   it 'returns [1, 1]' do
  #     expected_response = [1, 1]
  #     expect(subject).to eq expected_response
  #   end
  # end
  # context 'when attempt have values on different and exact positions ' do
  #   let(:secret_arr) { [1, 2, 1, 5, 2] }
  #   let(:attempt_arr) { [3, 1, 1, 5, 2] }

  #   it 'returns [3, 1]' do
  #     expected_response = [3, 1]
  #     expect(subject).to eq expected_response
  #   end
  # end
  context 'when attempt have values on different and exact positions' do
    let(:secret_arr) { [2, 2, 4, 1, 5] }
    let(:attempt_arr) { [1, 2, 3, 4, 5] }
    it 'returns [2, 2]' do
      expected_response = [2, 2]
      expect(subject).to eq expected_response
    end

    context 'with a value in different and exact position' do
      let(:secret_arr) { [2, 1, 4, 2, 5] }
      let(:attempt_arr) { [1, 1, 4, 1, 5] }

      it 'should returns [4, 0]' do
        expected_response = [3, 0]
        expect(subject).to eq expected_response
      end
    end
  end
end
