class Mastermind
  INDEX_EXACT_POSITION = 0
  INDEX_DIFFERENT_POSITION = 1

  def self.evaluate(secret, attempt)
    return [secret.length, 0] if secret == attempt

    identificate_exact_and_different_positions(secret, attempt)
  end

  def self.identificate_exact_and_different_positions(secret, attempt)
    response_array = [0, 0]

    attempt.each_with_index do |val, index|
      if secret[index] == val
        response_array[INDEX_EXACT_POSITION] += 1
      elsif in_other_position?(val, secret, attempt)
        response_array[INDEX_DIFFERENT_POSITION] += 1
      end
    end
    response_array
  end

  def self.in_other_position?(val, secret, attempt)
    result = false
    secret.each_with_index do |value, index|
      result = true if value == val && value != attempt[index]
    end
    result
  end

  # Generar un initialize para los valores de secret y attempt y asi no pasarlos 
  # de un metodo a otro.

  # Limpiar los registros correctos, para la segunda vez que lo recorramos para buscar
  # los registros que no coincidan, no tener que recorrer todos los registros y quedarnos solo
  # con los pocos relevantes para el segundo caso.

end
