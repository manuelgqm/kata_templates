require './implementation'

# 2 - 2 - 4 - 1 - 5 -> original
#
# 1 - 2 - 3 - 4 - 5 -> 2, 2
# 2 - 1 - 4 - 1 - 5 -> 4, 0 *
# 2 - 2 - 4 - 1 - 5 -> 5, 0
#
describe 'Mastermind' do

  subject {
    Mastermind.play(code, input)
  }

  context 'when input is same as the code' do
    context 'and input has only one position' do
      let(:input) { [1] }
      let(:code) { [1] }
      it 'should return first output position with one' do
        expected_result = [1, 0]
        is_expected.to eq(expected_result)
      end
    end

    context 'and input has more than one position' do
      let(:input) { [1, 3] }
      let(:code) { [1, 3] }
      it 'should return first output position with n numbers from input' do
        expected_result = [2, 0]
        is_expected.to eq(expected_result)
      end
    end

  end

  context 'when input is different than the code' do
    context 'all input values are contained in the code but not in the same order' do
      let(:input) { [1, 2] }
      let(:code) { [2, 1] }
      it 'counts two numbers of present values that are not in correct position' do
        expected_result = [0, 2]
        is_expected.to eq(expected_result)
      end
    end

    context 'some input values are contained in the code but not in the same order' do
      let(:input) { [1, 2] }
      let(:code) { [3, 1] }
      it 'counts the one number that is present values but not in correct position' do
        expected_result = [0, 1]
        is_expected.to eq(expected_result)
      end
    end
    
    context 'all present values are in correct position and others are not present'  do
      let(:input) { [1, 2, 4] }
      let(:code) { [1, 2, 5] }
      it 'counts two numbers that are present and in correct position' do
        expected_result = [2, 0]
        is_expected.to eq(expected_result)
      end
    end

    context 'when present values are both, in correct and incorrect position' do
      let(:input) { [1, 3, 2] }
      let(:code) { [1, 2, 3] }
      it 'counts the one number that is present values but not in correct position' do
        expected_result = [1, 2]
        is_expected.to eq(expected_result)
      end
    end
  end
  
end
