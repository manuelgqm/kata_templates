class Mastermind
  class << self
    def play(code, input)
      correct_positioned = collect_correct_positioned(code, input)
    
      only_present_count = count_present(code, input - correct_positioned)

      result = [correct_positioned.count, only_present_count]
    end

    private

    def count_present(code, input)
      matches = 0
      code.each do |x|
        matches += 1 if input.include? x
      end

      return matches
    end

    def collect_correct_positioned(code, input)
      input.filter.with_index do |item, index|
        code[index] == item
      end
    end
  end
end
