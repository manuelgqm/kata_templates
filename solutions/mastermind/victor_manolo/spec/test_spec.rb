require './implementation'

describe 'A test suite' do
  it 'failing test' do
    expected_combination = [2, 2, 4, 1, 5]
    proposal_combination = [3, 3, 3, 3, 3]

    master_mind = MasterMind.new(expected_combination, proposal_combination)
    response = master_mind.compare
    expect(response).to eq [0, 0]
  end
end
