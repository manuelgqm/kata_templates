import Mastermind from "mastermind"

describe("The Masterming Game", () => {
  it("Can set a secret key to discover", () => {
    const secretColors = ["red", "blue", "yellow", "black", "pink"]
    const mastermind = new Mastermind(secretColors)
    
    expect(mastermind.getSecretCode()).toEqual(secretColors)
  })

  it("Can set a different keys to discover", () => {
    const secretColors = ["black", "blue", "yellow", "black", "pink"]
    const mastermind = new Mastermind(secretColors)
    
    expect(mastermind.getSecretCode()).toBe(secretColors)
  })

  it("doesn't match any colour", () => {
    const secretColors = ["red", "blue", "yellow", "black", "pink"]
    const solutionAttempt = ["black", "black", "black", "pink", "black"]
    const mastermind = new Mastermind(secretColors)

    expect(mastermind.checkSolution(solutionAttempt)).toEqual("0,0")
  })

  it("match a well one placed color but none misplaced colors", () => {
    const secretColors = ["red", "blue", "yellow", "black", "pink"]
    const solutionAttempt = ["black", "black", "black", "black", "black"]
    const mastermind = new Mastermind(secretColors)

    expect(mastermind.checkSolution(solutionAttempt)).toEqual("1,0")
  })

})
