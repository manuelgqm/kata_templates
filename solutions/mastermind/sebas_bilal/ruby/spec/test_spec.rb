require './implementation'

describe 'Mastermind' do
  subject do
    Mastermind.evaluate(secret_key, attempt)
  end

  let(:secret_key) { [1] }
  let(:attempt) { [1] }

  context 'when attempt is evaluated against secret_key' do
    let(:secret_key) { [1, 2] }
    let(:attempt) { [1, 2] }

    context 'when attempt is correct' do
      it 'returns [2,0]' do
        is_expected.to eq([2, 0])
      end
    end
    context 'when attempt is incorrect' do
      context 'whith correct values in different positions' do
        let(:attempt) { [2, 1] }

        it 'returns [0,2]' do
          is_expected.to eq([0, 2])
        end
      end

      context 'with all values different' do
        let(:attempt) { [3, 4] }

        it 'returns [0,0]' do
          is_expected.to eq([0, 0])
        end
      end

      context 'with some values correct and others incorrect' do
        let(:attempt) { [1, 1] }

        it 'returns [1,0]' do
          is_expected.to eq([1, 0])
        end
      end
    end
  end

  # Controlar que se manden la misma cantidad de registros

  # Controlar valores nulos tambien -> nulo = incorrecto

end
