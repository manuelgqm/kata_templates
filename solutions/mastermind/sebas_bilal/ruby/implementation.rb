class Mastermind
  def self.evaluate(secret_key, attempt)
    return [secret_key.length, 0] if secret_key == attempt

    # require 'byebug'; byebug
    # if attempt == attempt | secret_key
    # [exact_position(secret_key, attempt), different_position(secret_key, attempt)]
    # else
    #   [0, 0]
    # end
  end

  # replantear para que este metodo haga ya todo, porque podriamos poner el include
  # en caso de que al recorrer o comprobar el registro, si no esta en la misma posicion,
  # hacer un include y ya, si esta pues que sume 1 a la derecha.
  def self.exact_position(secret_key, attempt)
    number_correct_position = 0
    secret_key.each_with_index do |val, index|
      number_correct_position+=1 if attempt[index] == val
    end
    number_correct_position
  end

  def self.different_position(secret_key, attempt)
    number_of_elements_in_other_possition = 0
    attempt.each{ |number|
      number_of_elements_in_other_possition += 1 if secret_key.include?(number)
    }
  end
end
