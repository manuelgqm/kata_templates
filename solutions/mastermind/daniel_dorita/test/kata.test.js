import Mastermind from "mastermind"

describe("The Masterming Game", () => {

  let secretColors = ["black", "blue", "yellow", "black", "pink"]

  it("Can set a secret key to discover", () => {
    const mastermind = new Mastermind(secretColors)

    expect(mastermind.getSecretCode()).toEqual(secretColors)
  })

  it("Gives to user an error to check length", () => {
    const solutionAttempt = ["black", "black", "black", "pink"]
    const mastermind = new Mastermind(secretColors)

    expect(mastermind.checkSolution(solutionAttempt)).toEqual('Validate input length')
  })

  it("doesn't match any colour", () => {
    const solutionAttempt = ["pink", "black", "black", "pink", "black"]
    const mastermind = new Mastermind(secretColors)

    expect(mastermind.checkSolution(solutionAttempt)).toEqual([0,0])
  })

  it("match all well placed colors", () => {
    const solutionAttempt = ["black", "blue", "yellow", "black", "pink"]
    const mastermind = new Mastermind(secretColors)

    expect(mastermind.checkSolution(solutionAttempt)).toEqual([5,0])
  })

})