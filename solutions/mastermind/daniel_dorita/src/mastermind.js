export default class Mastermind {
  constructor(secretColours) {
    this.secretCode = secretColours
  }

  getSecretCode(){
    return this.secretCode
  }

  checkSolution(solutionAttempt){
    
    let solution

    if (solutionAttempt.length == this.secretCode.length){

      let wellPlaced = this.validateWellPlacedColors(solutionAttempt)
      let notInOrder = 0
      solution = [wellPlaced,  notInOrder]

    } else{
      solution = "Validate input length"
    }

    return solution
  }

  validateWellPlacedColors(solutionAttempt){

    let matches = solutionAttempt.map((e,i) => e == this.secretCode[i])

    return matches.reduce((a, b) => a+b, 0)

  }

}